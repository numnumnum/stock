import joblib
import keras
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from utils import gen_data


class ReadPolicy:
    def __init__(self, name):
        self.name = name
        self.idx = None
        self.model = None
        self.sc = None
        self.in_features = None
        self.out_feature = None
        self.window = None

    def load_model(self, save_path=None):
        if not save_path:
            save_path = f'models/{self.name}.h5'
        self.model = keras.models.load_model(save_path)
        return self

    def load_scaled(self, save_path=None):
        if not save_path:
            save_path = f'pkl/{self.name}.pkl'
        self.sc = joblib.load(save_path)
        return self

    def load_params(self, save_path=None):
        """
        params = {
            "in_features": self.in_features,
            "out_feature": self.target,
            "window": self.window
        }
        :param save_path:
        :return:
        """
        if not save_path:
            save_path = f'params/{self.name}.pkl'
        params = joblib.load(save_path)
        self.window = params.get('window')
        self.in_features = params.get('in_features')
        self.out_feature = params.get('out_feature')
        self.idx = self.in_features.index(self.out_feature)
        return self

    def predict(self, dataframe, target=None, days=3):
        dataframe.set_index('trade_date', inplace=True)

        out_feature = target or self.out_feature

        dataframe = dataframe[self.in_features]
        scaled_set = self.sc.transform(dataframe)

        x_, y_ = gen_data(scaled_set, self.window)
        x_ = list(x_)

        scaled_set = scaled_set.tolist()
        x_.append(scaled_set[-self.window:])

        y_pre = self.model.predict(np.array(x_))

        count = 0
        while count < days:
            scaled_set.append(y_pre[-1])
            x_.append(scaled_set[-self.window:])
            y_pre = self.model.predict(np.array(x_))
            count += 1

        y_pre = self.sc.inverse_transform(y_pre)
        y = y_pre[:, self.idx]
        y_real = dataframe[out_feature][self.window - 1:]

        plt.plot(y_real, label=f'real {out_feature} {self.name}', color='r')
        plt.plot(y, label=f'pre {out_feature} {self.name}', color='b')
        plt.legend()
        plt.show()


if __name__ == '__main__':
    name1 = 'sh601012'
    con = 'postgresql+psycopg2://postgres:admin@localhost:5432/adata'
    df = pd.read_sql(name1, con)
    policy = ReadPolicy(name1)
    policy.load_model().load_scaled().load_params()

    policy.predict(df[-30:], target='close', days=10)
