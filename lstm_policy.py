import joblib
import keras.layers
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from keras.layers import LSTM, Dropout, Dense
from sklearn.preprocessing import MinMaxScaler

from utils import gen_data


class LSTMPolicy:
    def __init__(self, name):
        self.sc = MinMaxScaler(feature_range=(-1, 1))
        self.name = name
        self.model = None
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        self.in_dim = None
        self.history = None
        self.y_pre = None
        self.target = None
        self.target_idx = None
        self.window = None
        self.in_features = None

    def make_dataset(self, dataframe, feature, max_test_per=0.1, max_test_num=20, window=8, target='close'):
        """
        制作数据集
        :param dataframe:
        :param feature:
        :param max_test_per:
        :param max_test_num:
        :param window:
        :param target:
        :return:
        """
        self.target = target
        self.target_idx = feature.index(self.target)
        self.window = window
        self.in_features = feature

        df = dataframe[feature]
        self.in_dim = len(feature)
        val_len = min(int(len(dataframe) * max_test_per), max_test_num)
        train_df = df[:-val_len]
        test_df = df[-val_len:]
        train_set = self.sc.fit_transform(train_df)
        test_set = self.sc.transform(test_df)
        self.x_train, self.y_train = gen_data(train_set, window_size=window)
        self.x_test, self.y_test = gen_data(test_set, window_size=window)
        return self

    def make_pre_dataset(self, dataframe):
        """
        制作预测用的数据
        :param dataframe:
        :return:
        """
        pre_set = self.sc.transform(dataframe)
        x, y = gen_data(pre_set, self.window)
        x = list(x)
        x.append(pre_set[-self.window:])
        return np.array(x)

    def make_net(self):
        """
        制作网络
        :return:
        """
        opt = tf.keras.optimizers.Adam(learning_rate=1e-3)

        self.model = keras.Sequential(
            [
                LSTM(256, return_sequences=True),
                Dropout(0.1),
                LSTM(128, return_sequences=True),
                Dropout(0.1),
                LSTM(64),
                Dense(self.in_dim)
            ]
        )
        self.model.compile(optimizer=opt, loss='mse', metrics=['mse'])
        return self

    def train(self, batch_size=32, epochs=60):
        self.history = self.model.fit(self.x_train, self.y_train,
                                      batch_size=batch_size, epochs=epochs,
                                      validation_data=(self.x_test, self.y_test))

    def draw_history(self):
        """
        画出训练集和验证集的损失
        :return:
        """
        loss = self.history.history['loss']
        val_loss = self.history.history['val_loss']
        plt.plot(loss, label='loss')
        plt.plot(val_loss, label='val_loss')
        plt.legend()
        plt.show()

    def save_model(self, save_path=None):
        """
        保存模型
        :param save_path:
        :return:
        """
        if not save_path:
            save_path = f'models/{self.name}.h5'
        self.model.save(save_path)
        return self

    def save_scaled(self, save_path=None):
        """
        保存归一化的对象
        :param save_path:
        :return:
        """
        if not save_path:
            save_path = f'pkl/{self.name}.pkl'
        joblib.dump(self.sc, save_path)

    def save_params(self):
        params = {
            "in_features": self.in_features,
            "out_feature": self.target,
            "window": self.window
        }
        joblib.dump(params, f'params/{self.name}.pkl')

    def predict(self, scaled_data=None):
        """
        预测
        :param scaled_data:
        :return:
        """
        y_pre = self.model.predict(scaled_data or self.x_test)
        self.y_pre = self.sc.inverse_transform(y_pre)

    def draw_predicate(self):
        y_pre = self.y_pre[:, self.target_idx]
        plt.plot(y_pre, label=f'y_pre {self.target}: {self.name}')
        y_test = self.sc.inverse_transform(self.y_test)[:, self.target_idx]
        plt.plot(y_test, label=f'y_real {self.target}: {self.name}')
        plt.legend()
        plt.show()


if __name__ == '__main__':
    con = 'postgresql+psycopg2://postgres:admin@localhost:5432/adata'
    s_name = 'sh601012'
    dd = pd.read_sql(s_name, con)
    sm = LSTMPolicy(s_name)
    _feature = ['close', 'open', 'high', 'vol', 'low', 'i_close', 'j']
    _target = 'close'
    sm.make_dataset(dd, _feature, target=_target, max_test_num=30, window=10)
    sm.make_net().train(32, 100)
    sm.draw_history()
    sm.save_model()
    sm.save_scaled()
    sm.save_params()

    sm.predict()
    sm.draw_predicate()
