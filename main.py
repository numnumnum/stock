from harmonic import *
from retesting.retesting import ReTesting

if __name__ == '__main__':
    df1 = read_data('601012.SH', '20200101')
    # print(df1.tail())
    # exit()
    # 002420
    path = r"C:\Users\o\PycharmProjects\stock\202212.csv"
    # df1 = pd.read_csv(path)
    # df1 = df1[df1['code'] == 'ETHUSDT']
    # df1 = df1.loc[df1.index[::-1]]
    # df1.set_index('trade_date', inplace=True)
    # df1.set_index('date', inplace=True)
    target = ['low', 'high']
    # df = df1[target].iloc[-260:]
    df = df1[target]
    print(df)
    # df = df.iloc[-100:]
    rs = run(df, cls=CarbUp, delta_d=0.2, delta_b=0.2, start=20, end=60, step=2)
    if rs.empty:
        print('empty')
        exit()
    cols = ['x_position', 'a_position', 'b_position', 'c_position', 'd_position']
    rs.to_csv('rs.csv')
    df1 = read_data('601012.SH', '20000101')
    retesting = ReTesting(original_data=df1, retesting_data=rs)
    ps = retesting.buy_points()
    retesting.retesting(ps)
