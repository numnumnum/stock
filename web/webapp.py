import pandas as pd
from flask import Flask, jsonify, request

from utils import engine, conn, td

app = Flask(__name__)


def get_cur_data(date='20200102'):
    import datetime

    d1 = datetime.datetime.strptime(date, '%Y%m%d')
    store_date = d1 - datetime.timedelta(days=1)
    store_date = datetime.datetime.strftime(store_date, '%Y%m%d')

    d2 = d1 + datetime.timedelta(days=1)
    d2 = datetime.datetime.strftime(d2, '%Y%m%d')

    d3 = d1 + datetime.timedelta(days=2)
    d3 = datetime.datetime.strftime(d3, '%Y%m%d')

    r_df1 = td.daily(date)

    r_df2 = td.daily(d2)
    r_df3 = td.daily(d3)

    cols = ['ts_code', 'pct_chg']
    r_df1 = r_df1[cols]
    r_df2 = r_df2[cols]
    r_df3 = r_df3[cols]

    pd.DataFrame(r_df1).rename(columns={"ts_code": "code", "pct_chg": "pct_chg1"}, inplace=True)
    pd.DataFrame(r_df2).rename(columns={"ts_code": "code", "pct_chg": "pct_chg2"}, inplace=True)
    pd.DataFrame(r_df3).rename(columns={"ts_code": "code", "pct_chg": "pct_chg3"}, inplace=True)

    r_df = pd.merge(r_df1, r_df2, how='left', on='code')
    r_df = pd.merge(r_df, r_df3, how='left', on='code')

    sql = f"select * from stock{store_date}"
    l_df = pd.read_sql(sql, conn)

    df = pd.merge(l_df, r_df, how='left', on='code')
    df.to_csv(f'{store_date}.csv', index=False)

    increase1 = df[(df['pct_chg1'] > 0) & (df['pre1'] > 0)]
    increase1_ = df[(df['pre1'] > 0)]

    percent1 = len(increase1) / len(increase1_)

    increase2 = df[(df['pct_chg2'] > 0) & (df['pre2'] > 0)]
    increase2_ = df[(df['pre2'] > 0)]

    percent2 = len(increase2) / len(increase2_)

    increase3 = df[(df['pct_chg3'] > 0) & (df['pre3'] > 0)]
    increase3_ = df[(df['pre3'] > 0)]

    percent3 = len(increase3) / len(increase3_)

    decrease1 = df[(df['pct_chg1'] < 0) & (df['pre1'] < 0)]
    decrease1_ = df[(df['pre1'] < 0)]

    percent1_ = len(decrease1) / len(decrease1_)

    decrease2 = df[(df['pct_chg2'] < 0) & (df['pre2'] < 0)]
    decrease2_ = df[(df['pre2'] < 0)]

    percent2_ = len(decrease2) / len(decrease2_)

    decrease3 = df[(df['pct_chg3'] < 0) & (df['pre3'] < 0)]
    decrease3_ = df[(df['pre3'] < 0)]

    percent3_ = len(decrease3) / len(decrease3_)

    i1 = f'{store_date} increase1: {percent1}'
    i2 = f'{store_date} increase2: {percent2}'
    i3 = f'{store_date} increase3: {percent3}'
    d1 = f'{store_date} decrease1: {percent1_}'
    d2 = f'{store_date} decrease2: {percent2_}'
    d3 = f'{store_date} decrease3: {percent3_}'

    total1 = percent1 + percent1_
    total2 = percent2 + percent2_
    total3 = percent3 + percent3_

    t1 = f'{store_date} total1: {total1}'
    t2 = f'{store_date} total2: {total2}'
    t3 = f'{store_date} total3: {total3}'

    summary = [i1, i2, i3, d1, d2, d3, t1, t2, t3]
    print(summary)


# @app.get('/stocks')
def daily():
    get_cur_data()
    return 'ok'


if __name__ == '__main__':
    # app.run()
    get_cur_data()
    get_cur_data('20221130')
    get_cur_data('20221201')
