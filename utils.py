# encoding: utf-8
import re
from functools import wraps

import numpy as np
import pandas as pd
import tushare as ts


def df_deal(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        name_map = {
            "trade_date": "date",
            "ts_code": "code"
        }
        columns = ['trade_date', 'close', 'open', 'low', 'vol', 'high']
        r_df = func(*args, **kwargs)
        r_df.dropna(inplace=True)
        n_df = r_df.reindex(columns, axis=1)
        n_df.rename(columns=name_map, inplace=True)
        n_df.set_index('date', inplace=True)
        return n_df[::-1]

    return wrap


class TushareData:
    def __init__(self, token):
        self.__token = token
        ts.set_token(self.__token)
        self.pro = ts.pro_api()
        self.start = '20210101'
        self.end = '20300231'
        self.stock_code = '600992.SZ'
        self.index_code = '399300.SZ'
        self.sh = '000001.SH'
        self.sz = '399001.SZ'

    def set_start_end(self, start, end):
        self.start = start
        self.end = end

    @df_deal
    def get_index_daily(self, ts_code=None, start=None, end=None):
        return self.pro.index_daily(ts_code=ts_code or self.index_code,
                                    start_date=start or self.start,
                                    end_date=end or self.end, adj='qfq', adjfactor=True)

    @df_deal
    def get_stock_daily(self, ts_code=None, start=None, end=None):
        return ts.pro_bar(ts_code=ts_code or self.stock_code,
                          start_date=start or self.start,
                          end_date=end or self.end, adj='qfq', adjfactor=True)

    def get_index_stock_daily(self, ts_code, start=None, end=None):
        index_df = self.pro.index_daily(ts_code=self.sh,
                                        start_date=start or self.start,
                                        end_date=end or self.end, adj='qfq', adjfactor=True)

        stock_df = ts.pro_bar(ts_code=ts_code or self.stock_code,
                              start_date=start or self.start,
                              end_date=end or self.end, adj='qfq', adjfactor=True)

        df1 = index_df[['trade_date', 'close', 'open', 'low', 'high', 'vol']][::-1]
        df2 = stock_df[['trade_date', 'close', 'open', 'low', 'high', 'vol', 'pct_chg']][::-1]
        index_map = {
            'trade_date': 'i_date',
            'close': 'i_close',
            'open': 'i_open',
            'low': 'i_low',
            'vol': 'i_vol',
            'high': 'i_high',
        }
        df1.rename(columns=index_map, inplace=True)
        if len(df2) < len(df1):
            df1 = df1[-len(df2):]
        df3 = pd.concat([df2, df1], axis=1)
        df3.set_index('trade_date', inplace=True)
        return df3

    def get_all_codes(self) -> pd.DataFrame:
        return self.pro.query('stock_basic', exchange='', list_status='L',
                              fields='ts_code,symbol,name,area,industry,list_date')

    def daily(self, date):
        df = self.pro.daily(trade_date=date)
        return df

    def trade(self) -> list:
        df = self.pro.trade_cal(exchange='', start_date='19900101', end_date='20300101')
        return df[df['is_open'] == 1]['cal_date'].tolist()


def trend_rate(l1: list, l2: list):
    l1 = list(l1)
    l2 = list(l2)
    l1_tomorrow = pd.Series(l1).shift(-1).dropna()
    l1_today = pd.Series(l1[:-1])
    r1 = (l1_today < l1_tomorrow)
    l2_tomorrow = pd.Series(l2).shift(-1).dropna()
    l2_today = pd.Series(l2[:-1])
    r2 = (l2_today < l2_tomorrow)
    r1 = np.array(r1)
    r2 = np.array(r2)
    same = list(r1 == r2)
    return same.count(True) / len(same)


def get_args():
    import argparse
    import datetime

    start_default = '20210101'
    end_default = str(datetime.datetime.today().date()).replace('-', '')
    process_default = 30

    parser = argparse.ArgumentParser()
    parser.add_argument('--start', type=str, default=start_default, help='开始日期')
    parser.add_argument('--end', type=str, default=end_default, help='结束日期')
    parser.add_argument('--process', type=int, default=process_default, help='进程数')
    parser.add_argument('--model_dir', type=str, default=process_default, help='进程数')
    args = parser.parse_args()
    return args


def table_stock_convert(ss: str, flag='t2s'):
    """
    str
    :param ss: str
    :param flag:
    't2s' 表名转股票名, sz000001 -> 000001SZ
    's2t' 股票名转表名, 000001SZ -> sz000001
    'auto' 自动转另一种 sz000001 -> 000001SZ, 000001SZ -> sz000001
    :return: str
    """
    nss = ''
    par1 = r'[0-9]{6}\.[a-zA-Z]{2}'
    par2 = r'[a-zA-Z]{2}[0-9]{6}'

    if flag == 'auto':
        if re.match(par1, ss):
            nss = ''.join(ss.split('.')[::-1]).lower()
        elif re.match(par2, ss):
            nss = (ss[2:] + '.' + ss[0:2]).upper()
    elif flag == 't2s':
        nss = ''.join(ss.split('.')[::-1]).lower()
    elif flag == 's2t':
        nss = (ss[2:] + '.' + ss[0:2]).upper()
    else:
        nss = ss

    return nss


def gen_data(arr, window_size):
    x = []
    y = []
    for i in range(window_size, len(arr)):
        x.append(arr[i - window_size: i])
        y.append(arr[i - 1])

    return np.array(x), np.array(y)


def date2timestamp(s_date):
    """
    年月日转时间戳
    :param s_date: 字符串, 如 19700101
    :return: 时间戳
    """
    import datetime
    curr = datetime.datetime.strptime(s_date, '%Y%m%d')
    flag = datetime.datetime.strptime('19700101', '%Y%m%d')
    return int(((curr - flag).total_seconds() - 60 * 60 * 8) * 1000)
