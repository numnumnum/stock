import multiprocessing

import joblib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from keras.layers import LSTM, Dropout, Dense
from sklearn.preprocessing import MinMaxScaler

from global_params import a_trade_dates, conn, back_conn


# MACD
def start(df,
          stock_name,
          val_df=None,
          cols=('close', 'open', 'vol', 'high', 'low', 'i_close', 'j'),
          target='close',
          epochs=60,
          cycle=20,
          batch_size=32,
          split_len=30,
          pre_days=3,
          draw=False
          ):
    gpus = tf.config.experimental.list_physical_devices(device_type='GPU')
    for gpu in gpus:
        tf.config.experimental.set_memory_growth(gpu, True)

    cols = list(cols)
    target_idx = cols.index(target)
    df = df[cols]

    length = len(df) - split_len

    train_df = df[:length]
    test_df = df[length:]

    sc = MinMaxScaler(feature_range=(0, 1))
    train_set = sc.fit_transform(train_df)
    test_set = sc.transform(test_df)

    train_x = []
    train_y = []

    for i in range(cycle, length):
        train_x.append(train_set[i - cycle: i, :])
        train_y.append(train_set[i, :])

    train_x = np.array(train_x)
    np.random.seed(1)
    np.random.shuffle(train_x)
    train_y = np.array(train_y)
    np.random.seed(1)
    np.random.shuffle(train_y)
    tf.random.set_seed(1)

    test_x = []
    test_y = []

    for i in range(cycle, len(test_set)):
        test_x.append(test_set[i - cycle: i, :])
        test_y.append(test_set[i, :])

    test_x = np.array(test_x)
    test_y = np.array(test_y)

    # callback = tf.keras.callbacks.ModelCheckpoint(f'models/{stock_name}', save_best_only=True)
    joblib.dump(sc, f'pkl/{stock_name}.pkl')

    model = tf.keras.Sequential(
        [
            LSTM(256, return_sequences=True, input_dim=len(cols)),
            Dropout(0.2),
            LSTM(128, return_sequences=True),
            Dropout(0.1),
            LSTM(64),
            Dropout(0.1),
            Dense(len(cols))
        ]
    )

    opt = tf.keras.optimizers.Adam(learning_rate=1e-3)
    model.compile(optimizer=opt, loss='mse', metrics=['mse'])
    history = model.fit(train_x, train_y, batch_size=batch_size, epochs=epochs,
                        validation_data=(test_x, test_y), callbacks=[])

    model_path = f'models/{stock_name}'
    model.save(model_path)
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    plt.plot(loss, color='y', label='loss')
    plt.plot(val_loss, color='g', label='val_loss')
    plt.legend()
    if draw:
        plt.show()

    test_x = list(test_x)
    test_x.append(test_set[-cycle:, :])

    y1 = model.predict(np.array(test_x))
    y1_ = sc.inverse_transform(y1)

    y2 = test_df.iloc[cycle:, target_idx]

    real_list = []
    window = y1[-cycle:, :].tolist()

    # 预测，n为预测的天数
    n = 0
    while n < pre_days:
        _pre = model.predict(np.array([window]))
        real_list.append(_pre)
        window.pop(0)
        window.append(_pre.tolist()[0])
        n += 1

    cc = sc.inverse_transform(np.array(real_list).reshape(-1, len(cols)))

    yy = np.concatenate([y1_, cc], axis=0)
    y_pre = yy[:, target_idx]
    y_pre = pd.Series(y_pre).shift(-1).dropna().values

    tags = cols[cols.index('i_close') + 1:]

    plt.plot(y2, color='r', label=f'real {stock_name} in: {tags} out: {target}')
    plt.plot(y_pre, color='b', label=f'pre {stock_name} in: {tags} out: {target}')
    plt.legend()
    if draw:
        plt.show()
    y_pre = pd.Series(list(y_pre)).shift(-1).dropna().to_list()

    _pre3 = y_pre[-1]
    _pre2 = y_pre[-2]
    _pre1 = y_pre[-3]
    _pre0 = y_pre[-4]

    real1 = .0
    real2 = .0
    real3 = .0
    cur_date = test_df.index[-1]
    summary = {
        "code": stock_name,
        "loss": loss[-1],
        "val_loss": val_loss[-1],
        "pre1": (_pre1 - _pre0) / _pre0,
        "pre2": (_pre2 - _pre1) / _pre1,
        "pre3": (_pre3 - _pre2) / _pre2,
        "real1": real1,
        "real2": real2,
        "real3": real3,
        'date': cur_date
    }

    if val_df is not None:
        pre_end_date = a_trade_dates[a_trade_dates.index(cur_date) + pre_days]
        real_1_3 = val_df[cur_date: pre_end_date]['pct_chg'].tolist()
        summary['real1'] = real_1_3[1]
        summary['real2'] = real_1_3[2]
        summary['real3'] = real_1_3[3]

    return summary


def run(table_name='sz000001', pre_date='20221118', draw=True, epochs=60):
    """
    单只票指定日期
    :param epochs:
    :param draw:
    :param table_name:
    :param pre_date:
    :return:
    """
    _val_df = pd.read_sql(table_name, conn, index_col='trade_date')
    _df = _val_df[: pre_date]
    return start(_df, table_name, val_df=_val_df, epochs=epochs, draw=draw)


def run_all(start_date, stocks=None, process=2, draw=True):
    d_idx = a_trade_dates.index(start_date)
    dates = a_trade_dates[d_idx:-3]
    if stocks is None:
        sql = "select tablename from pg_tables where schemaname in('adata','public')"
        tables_df = pd.read_sql(sql, conn)
        stocks = tables_df['tablename'].tolist()

    pool = multiprocessing.Pool(processes=process)

    for day in dates:
        tasks = []
        for stock in stocks:
            task = pool.apply_async(run, (stock, day, draw))
            tasks.append(task)

        results = []
        for _task in tasks:
            try:
                line = _task.get()
                results.append(line)
            except Exception as e:
                print(e)

        if results:
            rs_df = pd.DataFrame(results)
            rs_df = rs_df.set_index('code')
            rs_df.to_sql(f"stock{day}", back_conn, if_exists='replace')


if __name__ == '__main__':
    run_all('20221128', process=2, stocks=['sz000001', 'sz000002', 'sz000003', ])
