from inspect import isfunction

from .MyTT import *


# IF()

def IFN(X, A, B):
    """
    根据条件求不同的值,同IF判断相反.
     用法:
      IFN(X,A,B)若X不为0则返回B,否则返回A
     例如:
      IFN(CLOSE>OPEN,HIGH,LOW)表示该周期收阴则返回最高值,否则返回最低值
    :param X:
    :param A:
    :param B:
    :return:
    """
    return IF(X, B, A)


def IFC(X, A, B):
    """
    根据条件求不同的值,可中止.
    用法:
     IFC(X,A,B)若X不为0则返回A,否则返回B.IFC与IF函数的区别:根据X的值来选择性执行A、B表达式.
    例如:
     IFC(CLOSE>OPEN,HIGH,TESTSKIP(1));L;表示当日收阳则返回最高值,
     并执行下一句"L;",否则退出公式计算
    :param X: bool序列
    :param A: 序列
    :param B: 函数
    :return: 序列或者函数名
    """
    B = B if isfunction(B) else A
    return np.where(X, A, B)


# VALUEWHEN(COND,X)

def TESTSKIP(A):
    """
    TESTSKIP(A):满足A则直接返回.
    用法:
     TESTSKIP(A)
     表示如果满足条件A则该公式直接返回,不再计算接下来的表达式 注意:A为非序列数据,只取最后一个数据
    :param A:
    :return:
    """
    if A:
        return
