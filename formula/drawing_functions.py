from .MyTT import *


def PLOYLINE(COND, PRICE) -> np.ndarray:
    """
    绘制折线段.
    用法:
     PLOYLINE(COND,PRICE),当COND条件满足时,以PRICE位置为顶点画折线连接.
    例如:
     PLOYLINE(HIGH>=HHV(HIGH,20),HIGH)表示在创20天新高点之间画折线
    :param COND:
    :param PRICE:
    :return:
    """
    res = [0.] * len(PRICE)
    last_idx, last = 0, 0.

    for cur_idx, cur in enumerate(PRICE):
        if COND[cur_idx]:
            res[cur_idx] = cur
            if last_idx + 1 < cur_idx:
                res[last_idx: cur_idx + 1] = np.linspace(last, cur, cur_idx - last_idx + 1)
            last, last_idx = cur, cur_idx

    return np.array(res)


def DRAWSL(COND, PRICE, SLOPE, LEN, DIRECT):
    """
    绘制斜线.
    用法:
     DRAWSL(COND,PRICE,SLOPE,LEN,DIRECT),当COND条件满足时,在PRICE位置画斜线,
     SLOPE为斜率,LEN为长度,DIRECT为0向右延伸,1向左延伸,2双向延伸.
    注意:
    1.K线间的纵向高度差为SLOPE;
    2.SLOPE为0时,为水平线;
    3.SLOPE为10000时,为垂直线,LEN为向上的像素高度,DIRECT表示向上或向下延伸;
    4.SLOPE和LEN支持变量;
    :param COND: 条件序列
    :param PRICE: 价格序列
    :param SLOPE: SLOPE为10000时,为垂直线,其他为水平线
    :param LEN: 为单位像素，1为1个周期，2为2个周期
    :param DIRECT: 水平时0向右延伸,1向左延伸,2双向延伸
    :return: 水平方向时返回[1,2,3,4]，垂直方向时[[1,2], [2.2, 3.2], [3.1, 4.1], [2.8, 3.8]]
    """

    length = len(COND)
    res_x = [np.NAN] * length
    # res_y, 元素第一个值为起点，第二个元素为终点
    res_y = [[0.0, ] for i in range(length)]

    def draw_x():
        """
        画水平方向的图
        :return: ndarray
        """
        for i, cond in enumerate(COND):
            if cond:
                left = i - LEN if i - LEN > 0 else 0
                right = i + LEN + 1 if i + LEN < length else length
                if DIRECT == 0:
                    res_x[i: right] = [PRICE[i]] * (right - i)
                elif DIRECT == 1:
                    res_x[left: i] = [PRICE[i]] * (i - left)
                elif DIRECT == 2:
                    res_x[left: right] = [PRICE[i]] * (right - left)
                res_x[i] = PRICE[i]
        return np.array(res_x)

    def draw_y():
        """
        画垂直方向的图
        :return: 2维ndarray
        """
        for i, cond in enumerate(COND):
            if cond:
                res_y[i][0] = PRICE[i]
                if DIRECT == 0 or DIRECT == 2:
                    res_y[i].append(PRICE[i] + LEN)
                elif DIRECT == 1:
                    res_y[i].append(PRICE[i] - LEN if PRICE[i] - LEN >= 0 else 0)

        return res_y

    res = draw_y() if SLOPE == 10000 else draw_x()
    return np.array(res)


def DRAWKLINE(HIGH, OPEN, LOW, CLOSE):
    """
    DRAWKLINE(HIGH,OPEN,LOW,CLOSE).
    用法:
     以HIGH为最高价,OPEN为开盘价,LOW为最低,CLOSE收盘画K线
    :param HIGH: 最高价序列
    :param OPEN: 开盘价序列
    :param LOW: 最低价序列
    :param CLOSE: 收盘价序列
    :return: 字典
    """
    data = pd.DataFrame({
        "high": HIGH,
        "open": OPEN,
        "low": LOW,
        "close": CLOSE
    })
    return {
        "data": data,
        "type": None,
        "width": None
    }


def STICKLINE(COND, PRICE1, PRICE2, WIDTH, EMPTY):
    """
    绘制柱线.
    用法:
     STICKLINE(COND,PRICE1,PRICE2,WIDTH,EMPTY),当COND条件满足时,
     在PRICE1和PRICE2位置之间画柱状线,宽度为WIDTH(10为标准间距),
     EMPTY为0画实心柱,-1画虚线空心柱,否则画实线空心柱.
    例如:
     STICKLINE(CLOSE>OPEN,CLOSE,OPEN,0.8,1)表示画K线中阳线的空心柱体部分.
    :param COND: 条件序列
    :param PRICE1: 序列
    :param PRICE2: 序列
    :param WIDTH: 数值，像素宽度
    :param EMPTY: EMPTY为0画实心柱,-1画虚线空心柱,否则画实线空心柱.
    :return: 字典
    """

    opn = np.where(COND, PRICE1, 0)
    close = np.where(COND, PRICE2, 0)

    return {
        "data": pd.DataFrame({
            "low": opn,
            "high": close,
            "close": close,
            "open": opn,
        }),
        "width": WIDTH,
        "type": EMPTY
    }


def DRAWICON(COND, PRICE, TYPE):
    """
    绘制小图标.
    用法:
     DRAWICON(COND,PRICE,TYPE),当COND条件满足时,在PRICE位置画TYPE号图标(TYPE为1--46).
    例如:
     DRAWICON(CLOSE>OPEN,LOW,1)表示当收阳时在最低价位置画1号图标.
    :param COND: 条件序列
    :param PRICE: 价格序列
    :param TYPE: 图类型
    :return: 字典
    """
    COND = pd.Series(COND)
    PRICE = pd.Series(PRICE)

    PRICE[~COND] = 0

    return {
        "data": PRICE,
        "type": TYPE,
        "width": None
    }


def DRAWTEXT(COND, PRICE, TEXT):
    """
    显示文字;在指标排序中显示字符串栏目.
    用法:
     DRAWTEXT(COND,PRICE,TEXT),当COND条件满足时,在PRICE位置书写文字TEXT.
    例如:
     DRAWTEXT(CLOSE/OPEN>1.08,LOW,'大阳线')表示当日实体阳线大于8%时在最低价位置显示'大阳线'字样.
    :param COND: 序列
    :param PRICE: 序列
    :param TEXT: 字符串
    :return: 字典
    """
    cond = pd.Series(COND)
    price = pd.Series(PRICE)
    price[~cond] = np.NAN
    return {
        "price": price,
        "text": TEXT
    }


def DRAWTEXT_FIX(COND, X, Y, TYPE, TEXT):
    """
    固定位置显示文字;在指标排序中显示字符串栏目.
    用法:
     DRAWTEXT_FIX(COND,X,Y,TYPE,TEXT),当COND条件满足时,
     在当前指标窗口内(X,Y)位置书写文字TEXT,X,Y为书写点在窗口中相对于左上角的百分比,
     TYPE:0为左对齐,1为右对齐.
    例如:
     DRAWTEXT_FIX(CURRBARSCOUNT=1 AND CLOSE/OPEN>1.08,0.5,0.5,0,'大阳线')
     表示最后一个交易日实体阳线大于8%时在窗口中间位置显示'大阳线'字样.
    :param COND:
    :param X:
    :param Y:
    :param TYPE:
    :param TEXT:
    :return:
    """


def DRAWNUMBER(COND, PRICE, NUMBER):
    """
    画出数字.
    用法:
     DRAWNUMBER(COND,PRICE,NUMBER),当COND条件满足时,在PRICE位置书写数字NUMBER.
    例如:
     DRAWNUMBER(CLOSE/OPEN>1.08,LOW,C)表示当日实体阳线大于8%时在最低价位置显示收盘价.
    :param COND:
    :param PRICE:
    :param NUMBER:
    :return:
    """
    cond = pd.Series(COND)
    price = pd.Series(PRICE)
    number = pd.Series(NUMBER)
    price[~cond] = 0
    number[~cond] = 0
    return {
        "position": price,
        "number": number
    }


def DRAWNUMBER_FIX(COND, X, Y, TYPE, NUMBER):
    """
    固定位置显示数字.
    # 理解的是在窗口内第一个条件成立的时候的在X,Y位置书写 NUMBER
    用法:
     DRAWNUMBER_FIX(COND,X,Y,TYPE,NUMBER),当COND条件满足时,
     在当前指标窗口内(X,Y)位置书写数字NUMBER,X,Y为书写点在窗口中
     相对于左上角的百分比,TYPE:0为左对齐,1为右对齐.
    例如:
     DRAWNUMBER_FIX(CURRBARSCOUNT=1 AND CLOSE/OPEN>1.08,0.5,0.5,0,C)
     表示最后一个交易日实体阳线大于8%时在窗口中间位置显示收盘价.
    :param COND: 序列
    :param X: 数字
    :param Y: 数字
    :param TYPE: 0为左对齐,1为右对齐
    :param NUMBER: 序列
    :return:
    """
    number = 0
    for i, c in enumerate(COND):
        if c:
            number = NUMBER[i]

    if number == 0:
        return

    return {
        "x": X,
        "y": Y,
        "number": number,
        "type": TYPE
    }


def RGB(Red, Green, Blue) -> int:
    """
    将自定颜色作为数值输出.
    用法:
     RGB(Red,Green,Blue),输出为0XBBGGRR,参数使用十进制数,
     范围为0至255,以最近一个周期数值为准.
    例如:
     RGB(0,0,255)输出结果为16进制的 0XFF0000.
    :param Red: 整型
    :param Green: 整型
    :param Blue: 整型
    :return: 常数，如 RGB(30,20,10) 返回 660510
    """
    r = hex(Red)
    g = hex(Green)
    b = hex(Blue)
    rs = f"0X{b}{g}{r}".replace("0x", "")
    return int(rs, 16)


def DRAWBAND(VAL1, COLOR1, VAL2, COLOR2):
    """
    画出带状线.
    用法:
     DRAWBAND(VAL1,COLOR1,VAL2,COLOR2),当VAL1>VAL2时,
     在VAL1和VAL2之间填充COLOR1;当VAL1<VAL2时,填充COLOR2,
     这里的颜色均使用RGB函数计算得到.
    例如:
     DRAWBAND(OPEN,RGB(0,224,224),CLOSE,RGB(255,96,96))
    :param VAL1:
    :param COLOR1:
    :param VAL2:
    :param COLOR2:
    :return:
    """


def DRAWBMP(COND, PRICE, bmp_file):
    """
    画出图片.
    用法:
     DRAWBMP(COND,PRICE,'Bmp文件名'),当条件COND满足时,
     在PRICE位置画T0002目录下面的signals目录下面的"Bmp文件名";
    例如:
     DRAWBMP(O>C,CLOSE,'高开');
    :param COND:
    :param PRICE:
    :param bmp_file:
    :return:
    """


def DRAWGBK(COND, COLOR1, COLOR2, HXJJ, BMP_PNG, UP_BMP_PNG):
    """
    填充背景.
    用法:
     DRAWGBK(COND,COLOR1,COLOR2,是否横向渐进,'BMP或PNG文件名',是否拉升BMP或PNG文件名),
     当条件COND满足时填充背景,如果COLOR1和COLOR2有一个不为0,则以COLOR1到COLOR2的渐变色为背景,
     否则画T0002目录下面的signals目录下面的'Bmp或Png文件名'(优先使用BMP文件).
    例如:
     DRAWGBK(O>C,RGB(0,255,0),RGB(255,0,0),0,'背景图',0);
    :param COND:
    :param COLOR1:
    :param COLOR2:
    :param HXJJ:
    :param BMP_PNG:
    :param UP_BMP_PNG:
    :return:
    """


def DRAWRECTREL(LEFT, TOP, RIGHT, BOTTOM, COLOR):
    """
    相对位置上画矩形.
    用法:
     DRAWRECTREL(LEFT,TOP,RIGHT,BOTTOM,COLOR),以图形窗口(LEFT,TOP)为左上角,
     (RIGHT,BOTTOM)为右下角绘制矩形,坐标单位是窗口沿水平和垂直方向的1/1000,
     取值范围是0—999,超出范围则可能显示在图形窗口外,矩形中间填充颜色COLOR,COLOR为0表示不填充.
    例如:
     DRAWRECTREL(0,0,500,500,RGB(255,255,0))表示在图形最左上部1/4位置用黄色绘制矩形.
    :param LEFT:
    :param TOP:
    :param RIGHT:
    :param BOTTOM:
    :param COLOR:
    :return:
    """
