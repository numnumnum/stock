import math
import random

from .MyTT import *


# MAX()
# MIN()
def MAX6(A, B, C, D, E, F) -> np.ndarray:
    """
    求6个参数中的最大值.
    用法:
     MAX6(A,B,C,D,E,F)返回较大值
    :param A:
    :param B:
    :param C:
    :param D:
    :param E:
    :param F:
    :return:
    """
    len_info = [len(pd.Series(x)) for x in [A, B, C, D, E, F]]

    max_len = 1
    if len(set(len_info)) > 1:
        max_len = max(len_info)

    new_li = []
    for i in [A, B, C, D, E, F]:
        new_li.append([i] * max_len) if isinstance(i, int) else new_li.append(i.values)

    return pd.DataFrame(new_li).max(axis=0).values


def MIN6(A, B, C, D, E, F) -> np.ndarray:
    """
    求6个参数中的最小值.
    用法:
     MIN6(A,B,C,D,E,F)返回较小值
    :param A:
    :param B:
    :param C:
    :param D:
    :param E:
    :param F:
    :return:
    """
    max_len = 1
    new_li = []
    len_info = [len(pd.Series(x)) for x in [A, B, C, D, E, F]]

    if len(set(len_info)) > 1:
        max_len = max(len_info)

    for i in [A, B, C, D, E, F]:
        new_li.append([i] * max_len) if isinstance(i, int) else new_li.append(i.values)

    return pd.DataFrame(new_li).min(axis=0).values


def ACOS(X) -> np.ndarray:
    """
    反余弦值.
    用法:
     ACOS(X)返回X的反余弦值
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.acos).values


def ASIN(X) -> np.ndarray:
    """
    反正弦值.
    用法:
     ASIN(X)返回X的反正弦值
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.asin).values


def ATAN(X) -> np.ndarray:
    """
    反正切值.
    用法:
    ATAN(X)返回X的反正切值
    :param X: 一个数字的序列，double型
    :return: 返回序列对应元素的atan值
    """
    return pd.Series(X).apply(math.atan).values


def COS(X) -> np.ndarray:
    """
    余弦值.
    用法:
     COS(X)返回X的余弦值
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.cos).values


def SIN(X) -> np.ndarray:
    """
    正弦值.
    用法:
     SIN(X)返回X的正弦值
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.sin).values


def TAN(X) -> np.ndarray:
    """
    正切值.
    用法:
     TAN(X)返回X的正切值
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.tan).values


def EXP(X) -> np.ndarray:
    """
    指数.
    用法:
     EXP(X)为e的X次幂
    例如:
     EXP(CLOSE)返回e的CLOSE次幂
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.exp).values


def LN(X) -> np.ndarray:
    """
    求自然对数.
    用法:
     LN(X)以e为底的对数
    例如:
     LN(CLOSE)求收盘价的对数
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.log).values


def LOG(X) -> np.ndarray:
    """
    求10为底的对数.
    用法:
     LOG(X)取得X的对数
    例如:
     LOG(100)等于2
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.log10).values


def SQRT(X) -> np.ndarray:
    """
    开平方.
    用法:
     SQRT(X)为X的平方根
    例如:
     SQRT(CLOSE)收盘价的平方根
    :param X:
    :return:
    """
    return pd.Series(X).apply(math.sqrt).values


# ABS()

def POW(A, B) -> np.ndarray:
    """
    乘幂.
    用法:
     POW(A,B)返回A的B次幂
    例如:
     POW(CLOSE,3)求得收盘价的3次方
    :param A: 一个序列
    :param B: 整型
    :return: 一个序列
    """
    return pd.Series(A).apply(lambda x: x ** B).values


def CEILING(A) -> np.ndarray:
    """
    向上舍入.
    用法:
     CEILING(A)返回沿A数值增大方向最接近的整数
    例如:
     CEILING(12.3)求得13,CEILING(-3.5)求得-3
    :param A:
    :return:
    """
    return pd.Series(A).apply(math.ceil).values


def FLOOR(A) -> np.ndarray:
    """
    向下舍入.
    用法:
     FLOOR(A)返回沿A数值减小方向最接近的整数
    例如:
     FLOOR(12.3)求得12,FLOOR(-3.5)求得-4
    :param A:
    :return:
    """
    return pd.Series(A).apply(math.floor).values


def INTPART(A) -> np.ndarray:
    """
    取整.
    用法:
     INTPART(A)返回沿A绝对值减小方向最接近的整数
    例如:
     INTPART(12.3)求得12,INTPART(-3.5)求得-3
    :param A: 一个序列
    :return: 一个序列
    """
    return pd.Series(A).apply(lambda x: math.modf(x)[1]).values


def BETWEEN(A, B, C) -> np.ndarray:
    """
    介于.
    用法:
     BETWEEN(A,B,C)表示A处于B和C之间时返回1,B<A<C或C<A<B,否则返回0
    例如:
     BETWEEN(CLOSE,MA(CLOSE,10),MA(CLOSE,5))
     表示收盘价介于5日均线和10日均线之间
    :param A: 一个序列或数字
    :param B: 一个序列或数字
    :param C: 一个序列或数字
    :return: 一个序列
    """
    A = pd.Series(A)
    B = pd.Series(B)
    C = pd.Series(C)

    return A.between(B, C).values


def FRACPART(X) -> np.ndarray:
    """
    小数部分.
    用法:
     FRACPART(X),返回X的小数部分
    :param X: 一个序列
    :return: 一个序列
    """
    return pd.Series(X).apply(lambda x: math.modf(x)[0]).values


def ROUND(X) -> np.ndarray:
    """
    四舍五入.
    用法:
     ROUND(X),返回X四舍五入到个位的数值
    :param X: 一个序列
    :return: 一个序列
    """
    return pd.Series(X).apply(round).values


def ROUND2(X, N) -> np.ndarray:
    """
    四舍五入.
    用法:
     ROUND2(X,N),返回X四舍五入到N位小数的数值
    由于精度问题,数据越大误差可能越大
    :param X: 一个序列
    :param N: 四舍五入到N位小数的数值
    :return: 一个序列
    """
    return pd.Series(X).apply(lambda x: round(x, N)).values


def SIGN(X) -> np.ndarray:
    """
    取符号.
    用法:
     SIGN(X),返回X的符号.当X>0,X=0,X<0分别返回1,0,-1
    :param X: 序列
    :return: 序列
    """

    def flag(i):
        if i > 0:
            return 1
        elif i == 0:
            return 0
        else:
            return -1

    return pd.Series(X).apply(flag).values


def MOD(M, N) -> np.ndarray:
    """
    取模.
    用法:
     MOD(M,N),返回M关于N的模(M除以N的余数)
    例如:
     MOD(5,3)返回2 注意:公式系统对有效数字部分有要求,
     如果数字部分超过7-8位,会有精度丢失
    :param M: 一个序列
    :param N: 整型数字
    :return: 一个序列
    """
    return pd.Series(M).apply(lambda x: x % N).values


def RAND(N, length=0) -> np.ndarray:
    """
    取随机数.
    用法:
     RAND(N),返回一个范围在1-N的随机整数
    :param length: 如果N为int或者float则返回整个长度的随机数,
    length为必要参数，如果N为序列则length为无用参数
    :param N: 序列或者整数
    :return: 序列
    """
    if isinstance(N, int) or isinstance(N, float):
        return pd.Series([int(N)] * length).apply(lambda x: random.randint(1, int(x))).values

    return pd.Series(N).apply(lambda x: random.randint(1, int(x))).values


if __name__ == '__main__':
    rrr = MOD([1, 2, 3], 3)
    print(rrr)
