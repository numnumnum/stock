from functools import wraps

import pandas

from formula.errors import ParamsError


def unfinished_function(func):
    @wraps(func)
    def dec(*args, **kwargs):
        print(f"unfinished function {func.__name__}")
        res = func(*args, **kwargs)
        return res

    return dec


def check_ser_params_only(allow=(0, 1)):
    def wrap(func):
        @wraps(func)
        def dec(ser):
            elements = set(ser)
            if (elements | set(allow)) != set(allow):
                msg = f"function {func.__name__} params only {allow} is allowed"
                raise ParamsError(msg)
            _res = func(ser)
            return _res

        return dec

    return wrap


@unfinished_function
def te():
    pass


def to_ndarray(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        return pandas.array(func(*args, **kwargs))

    return wrap


if __name__ == '__main__':
    te()
