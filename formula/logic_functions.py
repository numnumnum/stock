from .MyTT import *

from .utils import check_ser_params_only


# CROSS(A,B)
# LONGCROSS()

def UPNDAY(X, M):
    """
    返回是否连涨周期数.
    用法:
     UPNDAY(CLOSE,M)
    表示连涨M个周期,M为常量
    :param X: 序列x
    :param M: M个周期
    :return: 序列
    """

    def is_true(li):
        """
        判断条件是否在周期内全部成立
        :param li:
        :return:
        """
        ans = {True}
        if set(li) == ans:
            return 1
        return 0

    up_day = pd.Series(X) > pd.Series(X).shift(1)
    return up_day.rolling(M).apply(is_true).fillna(0).values


def DOWNNDAY(X, M):
    """
    返回是否连跌周期.
    用法:
     DOWNNDAY(CLOSE,M)
    表示连跌M个周期,M为常量
    :param X:
    :param M:
    :return:
    """

    def is_true(li):
        """
        判断条件是否在周期内全部成立
        :param li:
        :return:
        """
        ans = {True}
        if set(li) == ans:
            return 1
        return 0

    down_day = pd.Series(X) < pd.Series(X).shift(1)
    return down_day.rolling(M).apply(is_true).fillna(0).values


def NDAY(X, Y, N):
    """
    返回是否持续存在X>Y
    用法:
     NDAY(CLOSE,OPEN,3)
    表示连续3日收阳线
    :param X:
    :param Y:
    :param N:
    :return:
    """
    X = pd.Series(X)
    Y = pd.Series(Y)

    def is_true(li):
        """
        判断条件是否在周期内全部成立
        :param li:
        :return:
        """
        ans = {True}
        if set(li) == ans:
            return 1
        return 0

    return (X > Y).rolling(N).apply(is_true).fillna(0).values


# EXIST(CLOSE>OPEN,10)


def EXISTR(X, A, B) -> np.ndarray:
    """
    EXISTR(X,A,B):是否存在(前几日到前几日间).
    例如:
    EXISTR(CLOSE>OPEN,10,5)
    表示从前10日内到前5日内存在着阳线
    若A为0,表示从第一天开始,B为0,表示到最后日止
    :param X: bool 序列
    :param A: 前A天
    :param B: 前B天
    :return: 序列
    """

    if A == B == 0:
        return np.array([0])

    lx = list(X)[::-1]

    def exist_true(li):
        if True in set(li):
            return 1
        return 0

    # if A == 0:
    res = [np.NAN] * len(lx)
    if A == 0:
        for i in range(len(lx)):
            cur = lx[i:]
            res[i] = exist_true(cur)

    elif B == 0:
        for i in range(len(lx)):
            cur = lx[:(i + 1)]
            res[i] = exist_true(cur)
    else:
        for i in range(len(lx)):
            cur = lx[B + i: A + i + 1]
            res[i] = exist_true(cur)

    return np.array(res[::-1])


# EVERY(CLOSE>OPEN,N)

# LAST()

@check_ser_params_only((0, 1, True, False))
def NOT(X):
    """
    求逻辑非.
    用法:
     NOT(X)返回非X,即当X=0时返回1,否则返回0
    例如:
     NOT(ISUP)表示平盘或收阴
    :param X: 0, 1 组成的序列
    :return: X元素取反
    """
    return np.logical_not(pd.Series(X))
