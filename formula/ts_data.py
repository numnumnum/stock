import datetime
import os.path

import pandas
import tushare as ts
import pandas as pd


class AData:
    def __init__(self, token="818878b9d9b32445d45f25168cf4d8fc0d0374db2b70d06067e592df"):
        self.token = token
        real_path = os.path.realpath(__file__)
        self.path = os.path.dirname(real_path)
        ts.set_token(self.token)
        self.pro = ts.pro_api()

    def get_all_codes_data(self, filename="all_codes.csv"):
        """
        查询当前所有正常上市交易的股票列表，保存到csv文件
        :param filename:
        :return:
        """

        save_path = os.path.join(self.path, "csv_data", filename)
        data = self.pro.stock_basic(exchange='', list_status='L')
        data.to_csv(save_path, index=False, encoding='utf-8')

    def get_module_info(self, filename="module_info.csv"):
        """
        查询板块信息，tushare目前没有权限，积分不够
        :param filename:
        :return:
        """
        save_path = os.path.join(self.path, "csv_data", filename)
        df = self.pro.daily_info(trade_date='20200320', exchange='SZ,SH', fields='trade_date,ts_name,pe')
        df.to_csv(save_path, index=False, encoding='utf-8')

    def get_trade_day(self) -> pandas.DataFrame:
        """
        求上一个交易日的日期
        :return: 返回上一个交易日日期字符串
        """
        today = datetime.datetime.today().strftime("%Y%m%d")
        df = self.pro.trade_cal(exchange='', start_date='19890101', end_date=today)
        return df


def date_sub(day1: str, day2: str):
    """
    计算两个日期的天数差 day1 - day2
    :param day1: 字符串day1
    :param day2: 字符串day2
    :return:
    """
    n = 0
    day1 = datetime.datetime.strptime(day1, '%Y%m%d').date()
    day2 = datetime.datetime.strptime(day2, '%Y%m%d').date()

    def days_diff(_day1, _day2):
        nonlocal n

        if _day1 == _day2:
            return n

        if _day1 > _day2:
            n += 1
            days_diff(_day1, _day2 + datetime.timedelta(days=1))
        else:
            n -= 1
            days_diff(_day1, _day2 - datetime.timedelta(days=1))

    days_diff(day1, day2)
    return n


if __name__ == '__main__':
    res = AData().get_trade_day()
    print(res)
