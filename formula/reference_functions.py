from .MyTT import *


def DRAWNULL():
    """
    返回无效数.
    用法:
     DRAWNULL
    例如:
     IF(CLOSE>REF(CLOSE,1),CLOSE,DRAWNULL)表示下跌时不画线
    :return: 空
    """

    return None


def ALIGNRIGHT(X) -> np.ndarray:
    """
    有效数据右对齐.
    用法:
    ALIGNRIGHT(X)有效数据向右移动,左边空出来的周期填充无效值
    例如:TC:=IF(CURRBARSCOUNT=2 || CURRBARSCOUNT=5,DRAWNULL,C);
    XC:ALIGNRIGHT(TC);删除了两天的收盘价,并将剩余数据右移
    :param X: 对应的交易数据的序列，比如收盘价，[6.62, 7.1, ... , 8.2]
    :return: 返回一个向右对齐的序列
    """
    _res = []
    data_len = len(X)
    for x in X:
        if x:
            _res.append(x)

    while len(_res) < data_len:
        _res.insert(0, None)

    return np.array(_res)


def BARSCOUNT(X) -> np.ndarray:
    """
    求总的周期数.
    用法:
    第一个有效数据到当前的间隔周期数
    注意:判断范围为指标或条件选股计算时送给公式的数据,
    如果给画线指标的数据少(比如没有按下箭头取更多K线)或给条件选股给的数据少,这个有效值也可能少
    :param X: 一个序列，包含所有时间 ['19901219', '19901220', ... '20221103']
    :return: 序列的有效周期
    """
    ser = [1] * len(X)
    return (pd.Series(ser).cumsum() - 1).values


def BARSTATUS(X) -> np.ndarray:
    """
    BARSTATUS 返回数据位置信息, 1表示第一根K线, 2表示最后一个数据, 0表示中间位置.
    例如:
    BARSTATUS=2 表示当天是该数据的最后一个周期.
    :param X: 股票交易日序列
    :return: 上市日为1，当日为2，中间为0，返回一个序列
    """
    # 获取股票的交易日期
    rs = [0] * len(X)
    if len(X) == 1:
        rs = [1]

    elif len(X) == 2:
        rs = [1, 2]
    else:
        rs[0] = 1
        rs[-1] = 2
    return np.array(rs)


def CURRBARSCOUNT(X) -> np.ndarray:
    """
    求到最后交易日的周期数.
    用法:
    CURRBARSCOUNT 求到最后交易日的周期数
    :param X: 股票交易的序列
    :return: 返回一个从大到小的数字序列
    """
    return np.array(list(range(1, len(X) + 1))[::-1])


def TOTALBARSCOUNT(X) -> np.ndarray:
    """
    求总的周期数.
    用法:
    TOTALBARSCOUNT 求总的周期数
    :param X: 股票交易数据序列
    :return: 整数序列
    """
    return np.array([len(X)] * len(X))


def ISLASTBAR(X) -> np.ndarray:
    """
    判断是否为最后一个周期.
    用法:
    ISLASTBAR 判断是否为最后一个周期
    :param X: 股票交易数据序列
    :return: 数值序列
    """
    rs = [0] * len(X)
    rs[-1] = 1
    return np.array(rs)


# BARSSINCEN(X)

def BARSSINCE(X) -> np.ndarray:
    """
    第一个条件成立到当前的周期数.
    用法:
    BARSSINCE(X):第一次X不为0到现在的周期数
    例如:
    BARSSINCE(HIGH>10)表示股价超过10元时到当前的周期数
    :param X: 一个条件bool序列
    :return: 一个数值序列
    """
    res = [0] * len(X)
    num = 0
    flag = False

    for i, x in enumerate(X):
        if x:
            flag = True

        if flag:
            res[i] = num
            num += 1

    return np.array(res)


# BARSLASTCOUNT()


# HHV()
# HHVBARS()

def HOD(X, N):
    """
    求高值名次.
    用法:
    HOD(X,N):求当前X数据是N周期内的第几个高值,N=0则从第一个有效值开始.
    例如:
    HOD(HIGH,20)返回是20日的第几个高价
    :param X: 一个序列
    :param N: 周期N整型
    :return: 一个序列
    """

    def hod(li):
        ll = list(li)
        n = ll[:].pop()
        ll.sort(reverse=True)
        return ll.index(n) + 1

    return pd.Series(X).rolling(N).apply(hod).values


# LLV()
# LLVBARS()

def LOD(X, N):
    """
    求低值名次.
    用法:
    LOD(X,N):求当前X数据是N周期内的第几个低值,N=0则从第一个有效值开始.
    例如:
    LOD(LOW,20)返回是20日的第几个低价
    :return: 序列
    """

    def lod(li):
        ll = list(li)
        cur = ll[:].pop()
        ll.sort()
        return ll.index(cur) + 1

    return pd.Series(X).rolling(N).apply(lod).values


def REVERSE(X):
    """
    求相反数.
    用法:
    REVERSE(X)返回-X.
    例如:
    REVERSE(CLOSE)返回-CLOSE
    :param X:
    :return:
    """
    return pd.Series([-_ for _ in X]).values


# REF()

def REFV(X, A):
    """
    引用若干周期前的数据(未作平滑处理).
    用法:
    REFV(X,A),引用A周期前的X值.A可以是变量.
    平滑处理:当引用不到数据时进行的操作.
    例如:
    REFV(CLOSE,BARSCOUNT(C)-1)表示第二根K线的收盘价.
    :param X: 序列
    :param A: 周期N整型
    :return: 序列
    """

    return pd.Series(X).shift(A).values


def REFDATE(X, A):
    """

    :param X:
    :param A:
    :return: 通达信没有结果
    """
    return None


def CALCSTOCKINDEX(code, name, line):
    """
    指标引用.
    用法:CALCSTOCKINDEX(品种代码,指标名称,指标线),返回该指标相应输出的计算值.
    例如:
    CALCSTOCKINDEX('SH600000','KDJ',3)表示上证600000的KDJ指标第3个输出即J之值,
    第一个参数可在前面加SZ(深市),SH(沪市),或市场_,,
    CALCSTOCKINDEX('47_IFL0','MACD',2)表示IFL0品种的MACD指标第2个输出值.

    注意:引用品种的对应周期的数据必须要先下载到本地
    :param code: 品种代码
    :param name: 指标名称
    :param line: 指标线
    :return: 通达信没有结果
    """

    return None


# SUM()
def MULAR(X, N):
    """
    求累乘.
    用法:
     MULAR(X,N),统计N周期中X的乘积.N=0则从第一个有效值开始.
    例如:
     MULAR(C/REF(C,1),0)表示统计从上市第一天以来的复利
    :param X: 序列
    :param N: N整型
    :return: 序列
    """

    def mul(li):
        rs = 1
        for i in li:
            rs *= i

        return rs

    if N == 0:
        return pd.Series(X).cumprod().values

    return pd.Series(X).rolling(N).apply(mul).values


# FILTER()

def FILTERX(X, N):
    """
    反向过滤连续出现的信号.
    用法:FILTERX(X,N):X满足条件后,将其前N周期内的数据置为0,N为常量.
    例如:
     FILTERX(CLOSE>OPEN,5)查找阳线,前5天内出现过的阳线不被记录在内
    :param X: bool序列
    :param N: 周期N整型
    :return: 序列
    """
    res = [0] * len(X)
    X = list(X)

    if len(X) == 1:
        return pd.Series(X).values

    for i in range(len(X)):
        print(f"X[{i}] => {X[i]}")
        if i == 0:
            if X[i]:
                res[0] = 1
                continue

        if X[i]:
            if i <= N:
                res[: i] = [0] * i
            else:
                res[i - N: i] = [0] * N

            res[i] = 1

    return res


def TFILT(X, D1, M1, D2, M2):
    """
    对指定时间段的数据进行过滤,该时间段以外的数据无效.
    用法:
     TFILT(X,D1,M1,D2,M2)
    例如:
     TFILT(CLOSE,1040101,1025,1040101,1345)
     表示在2004年1月1日的10:25到2004年1月1日的13:45的收盘价是有效的.
    周期以日为基本单位的,分时为0有效.
    :param X:
    :param D1:
    :param M1:
    :param D2:
    :param M2:
    :return:
    """


def TFILTER(C1, C2, N):
    """
    过滤连续出现的信号.
    用法:TFILTER(买入条件,卖出条件,N);过滤掉买入(卖出)信号发出后,
    下一个反向信号发出前的所有买入(卖出)信号.

    N=1表示仅对买入信号过滤;
    N=2表示仅对卖出信号过滤;
    N=0表示对买入和卖出信号都过滤,返回1,2表示买入或卖出条件成立;
    同一K线上只能有一个信号;

    例如:
    ENTERLONG:TFILTER(买入,卖出,1);
    EXITLONG:TFILTER(买入,卖出,2);
    :param C1:
    :param C2:
    :param N:
    :return:
    """
    C1 = pd.Series(C1)
    C2 = pd.Series(C2)


def TTFILTER(B1, S1, S2, B2, N):
    """
    按照开平配对等原则过滤不合理的信号.
    用法:TTFILTER(买入开仓,卖出平仓,卖出开仓,买入平仓,N);

    主要规则有:
    1.连续的同方向指令只有第一个有效,其他的将被过滤;
    2.交易信号必须配对出现(比如前面已经有了买开指令,则后面只允许出现卖平指令,其他的指令都被过滤掉);

    N=1表示仅对买入开仓信号过滤;
    N=2表示仅对卖出平仓信号过滤;
    N=3表示仅对卖出开仓信号过滤;
    N=4表示仅对买入平仓信号过滤;
    N=0表示都过滤,返回1,2,3,4分别表示对应的条件成立;
    同一K线上只能有一个信号;

    例如:
    ENTERLONG:TTFILTER(买入开仓,卖出平仓,卖出开仓,买入平仓,1);
    EXITLONG:TTFILTER(买入开仓,卖出平仓,卖出开仓,买入平仓,2);
    ENTERSHORT:TTFILTER(买入开仓,卖出平仓,卖出开仓,买入平仓,3);
    EXITSHORT:TTFILTER(买入开仓,卖出平仓,卖出开仓,买入平仓,4);
    :param B1:
    :param S1:
    :param S2:
    :param B2:
    :param N:
    :return:
    """


def TR(LOW, HIGH) -> np.ndarray:
    """
    求真实波幅.
    用法:
     TR,求真实波幅.
     例如:ATR:=MA(TR,10);
     表示求真实波幅的10周期均值
    :param HIGH: 最高价序列
    :return: 最高价 - 最低价
    """
    low = pd.Series(LOW)
    high = pd.Series(HIGH)
    return (high - low).values


def SUMBARS(X, A) -> np.ndarray:
    """
    向前累加到指定值到现在的周期数.
    用法:
     SUMBARS(X,A):将X向前累加直到大于等于A,返回这个区间的周期数
    例如:SUMBARS(VOL,CAPITAL)求完全换手到现在的周期数
    :param X: 数据序列
    :param A: 目标值
    :return: 整数序列
    """

    def count_until(li, target):
        ll = li[::-1]
        total = 0
        n = 0
        for i in range(len(ll)):
            if total >= target:
                break
            n += 1
            total += ll[i]

        if total < target:
            return 0

        return n

    res = [0] * len(X)
    lx = list(X)
    for j in range(1, len(lx)):
        res[j] = count_until(lx[0:j + 1], A)

    return np.array(res)


# MA()

# SMA()

def TMA(X, A, B) -> np.ndarray:
    """
    返回移动平均
     用法:
     TMA(X,A,B),A和B必须小于1,算法	Y=(A*Y'+B*X),其中Y'表示上一周期Y值.初值为X
    :param X:
    :param A:
    :param B:
    :return:
    """
    if A > 1 or B > 1:
        raise "TMA(X,A,B),A和B必须小于1"

    X = list(X)
    n = len(X)
    res = [0] * n

    for i in range(n):
        if i == 0:
            res[0] = X[0]
        else:
            res[i] = A * res[i - 1] + B * X[i]

    return np.array(res)


def MEMA(X, N):
    """
    返回平滑移动平均
    用法:
     MEMA(X,N):X的N日平滑移动平均,如Y=(X+Y'*(N-1))/N
     MEMA(X,N)相当于SMA(X,N,1)
    :param X:
    :param N:
    :return:
    """
    X = list(X)
    n = len(X)
    res = [0] * n

    for i in range(n):
        if i == 0:
            res[0] = X[0]
        else:
            res[i] = (X[i] + res[i - 1] * (N - 1)) / N

    return np.array(res)


# EMA()

# EXPMA()

def EXPMEMA(X, N):
    """
    返回指数平滑移动平均 y=[2 * x + (N - 1) * y' ]/ (N + 1)
    用法:
     EXPMEMA(X,N):X的N日指数平滑移动平均
     EXPMEMA同EMA(EXPMA)的差别在于他的起始值为一平滑值
    :param X:
    :param N:
    :return:
    """
    X = list(X)
    n = len(X)
    res = [0] * n

    for i in range(n):
        if i == 0:
            res[0] = X[0]
        else:
            res[i] = (X[i] * 2 + (N - 1) * res[i - 1]) / (N + 1)

    return np.array(res)


# WMA()

# DMA()

def AMA(X, A):
    """
    求自适应均线值.
    用法:
     AMA(X,A),A为自适应系数,必须小于1.
    算法:
     Y=Y'+A*(X-Y').初值为X
    :param X:
    :param A:
    :return:
    """
    X = list(X)
    n = len(X)
    res = [0] * n

    for i in range(n):
        if i == 0:
            res[0] = X[0]
        else:
            res[i] = res[i - 1] + A * (X[i] - res[i - 1])

    return np.array(res)


def XMA(X, N):
    """
    属于未来函数,返回偏移移动平均
    用法:
     XMA(X,N):X的N日偏移移动平均,用到了当日以后N/2日的数据,
     N支持变量,只供内部测试使用
    :param X:
    :param N:
    :return:
    """


def RANGE(A, B, C):
    """
    RANGE(A,B,C):A在B和C范围之间,B<A<C.
    用法:
     RANGE(A,B,C)表示A大于B同时小于C时返回1,否则返回0
    :param A: 序列A
    :param B: 序列B
    :param C: 序列C
    :return: bool序列
    """
    # 兼容RANGE(REF(XA_20,1),12.94,12.96) 格式
    if isinstance(B, float):
        B = [B] * len(A)
    if isinstance(C, float):
        C = [C] * len(A)
    a = pd.Series(A)
    b = pd.Series(B)
    c = pd.Series(C)
    return ((b < a) & (a < c)).values


# CONST()

def TOPRANGE(X) -> list:
    """
    当前值是近多少周期内的最大值.
    用法:
     TOPRANGE(X):X是近多少周期内X的最大值
    例如:
     TOPRANGE(HIGH)表示当前最高价是近多少周期内最高价的最大值
    :param X: 数值序列
    :return: top N序列
    """

    total = len(X)
    if total <= 1:
        return [0]

    def top_n(li: list) -> int:
        """
        li中最后一个元素在多少周期内是最大的
        :param li: 数值序列
        :return: 数值序列
        """
        cur = li[-1]
        n = 0
        for j in range(1, len(li)):
            if li[-j - 1] > cur:
                break
            else:
                n += 1

        return n

    res = [0] * total

    for i in range(1, total):
        res[i] = top_n(X[:i + 1])

    return res


def LOWRANGE(X):
    """
    当前值是近多少周期内的最小值.
    用法:
     LOWRANGE(X):X是近多少周期内X的最小值
    例如:
     LOWRANGE(LOW)表示当前最低价是近多少周期内最低价的最小值
    :param X: 数值序列
    :return: 数值序列
    """
    total = len(X)
    if total <= 1:
        return [0]

    def low_n(li: list) -> int:
        """
        li中最后一个元素在多少周期内是最小的
        :param li:
        :return:
        """
        cur = li[-1]
        n = 0
        for j in range(1, len(li)):
            if li[-j - 1] < cur:
                break
            else:
                n += 1

        return n

    res = [0] * total

    for i in range(1, total):
        res[i] = low_n(X[:i + 1])

    return res


def FINDHIGH(VAR, N, M, T):
    """
    N周期前的M周期内的第T个最大值.
    用法:
     FINDHIGH(VAR,N,M,T):VAR在N日前的M天内第T个最高价
    :param VAR:
    :param N:
    :param M:
    :param T:
    :return:
    """

    def top_n(li):
        nl = list(li)
        nl.sort(reverse=True)
        return nl[T - 1]

    return pd.Series(VAR).shift(N).rolling(M).apply(top_n, raw=True).values


def FINDHIGHBARS(VAR, N, M, T):
    """
    N周期前的M周期内的第T个最大值到当前周期的周期数.
    用法:
     FINDHIGHBARS(VAR,N,M,T):VAR在N日前的M天内第T个最高价到当前周期的周期数
    :param VAR:
    :param N:
    :param M:
    :param T:
    :return:
    """

    def top_n_index(li):
        nl = list(li)
        nl.sort(reverse=True)
        top_value = nl[T - 1]
        return list(li)[::-1].index(top_value)

    return pd.Series(VAR).shift(N).rolling(M).apply(top_n_index, raw=True).values + N


def FINDLOW(VAR, N, M, T):
    """
    N周期前的M周期内的第T个最小值.
    用法:
     FINDLOW(VAR,N,M,T):VAR在N日前的M天内第T个最低价
    :param VAR:
    :param N:
    :param M:
    :param T:
    :return:
    """

    def min_n(li):
        nl = list(li)
        nl.sort()
        return nl[T - 1]

    return pd.Series(VAR).shift(N).rolling(M).apply(min_n, raw=True).values


def FINDLOWBARS(VAR, N, M, T):
    """
    N周期前的M周期内的第T个最小值到当前周期的周期数.
    用法:
     FINDLOWBARS(VAR,N,M,T):VAR在N日前的M天内第T个最低价到当前周期的周期数.
    :param VAR:
    :param N:
    :param M:
    :param T:
    :return:
    """

    def min_n_index(li):
        nl = list(li)
        nl.sort()
        top_value = nl[T - 1]
        return list(li)[::-1].index(top_value)

    return pd.Series(VAR).shift(N).rolling(M).apply(min_n_index, raw=True).values + N


def EXTERNSTR(TYPE, ID):
    """
    EXTERNSTR(TYPE,ID)
    TYPE为1表示是系统保留数据,
    TYPE为0表示是自定义外部数据,读取signals目录下面的的extern_user.txt,
    请用自定义数据管理器来维护
     extern_user.txt为文本结构,如下 1|600717|1|好股|0.33
    市场(0:深圳,1:上海)|品种代码|数据号|文字串|数值 如果是导出格式,则不需要数据号
    :param TYPE:
    :param ID:
    :return:
    """


def EXTERNVALUE(TYPE, ID):
    """
    EXTERNVALUE(TYPE,ID),用法同EXTERNSTR类似
    :param TYPE:
    :param ID:
    :return:
    """


def SIGNALS_SYS():
    """
    引用自定义序列数据(系统)
    :return:
    """


def SIGNALS_USER(p, TYPE):
    """
    引用自定义序列数据.
    读取个人目录下的signals目录下面的[signals_user_?]目录,
    请用自定义数据管理器来维护.
    SIGNALS_USER(11,TYPE):表示读当前品种的11数据号的序列数据
    (自定义数据需按日期从小到大排序),TYPE:1表示做平滑处理,
    没有自定义数据的周期返回上一周期的值;0表示不做平滑处理;2表示没有数据则为0.
    :param p:
    :param TYPE:
    :return:
    """


def EXTDATA_USER(N, TYPE):
    """
    引用扩展数据.
    请用扩展数据管理器来设置和刷新数据.
    EXTDATA_USER(N,TYPE),N取(1-100),
    表示读当前品种的N号扩展序列数据,
    TYPE:1表示做平滑处理,没有数据的周期返回上一周期的值;
    0表示不做平滑处理;2表示没有数据则为0.
    :param N:
    :param TYPE:
    :return:
    """


def SPLIT(N, TYPE):
    """
    除权除息数据
    SPLIT(N,TYPE),取得之前第N次除息除权(送转股,分红)的除权除息数据,
    参数为0表示送转股的比例(送转/(送转+10)),1表示每股分红(分红/10)),
    例如:SPLIT(0,0)=0.5表示最近一次除权可能是10送10
    :param N:
    :param TYPE:
    :return:
    """


def SPLITBARS(N, TYPE):
    """
    除权除息到现在的周期数
    SPLITBARS(N,TYPE),取得之前第N次除权除息到当前的周期数,
    参数为0表示送转股,1表示分红,2表示送转股或分红,
    例如:SPLITBARS(0,0)=0表示当天发生除权
    :param N:
    :param TYPE:
    :return:
    """


def ZTPRICE(X, A):
    """
    返回涨停价
    用法:
     ZTPRICE(REF(CLOSE,1),0.1):按10%计算得到在昨收盘基础上的涨停价
     (对于复权序列K线,由于复权处理,根据前一天的收盘价计算结果可能与涨停价不符)
    比如:
     ZTPrice(REF(QHJSJ,1),0.1),得到期货的涨停价
    :param X:
    :param A:
    :return:
    """
    return REF(X, 1) * (1 + A)


def DTPRICE(X, A):
    """
    返回跌停价
    用法:
     DTPRICE(REF(CLOSE,1),0.1):按10%计算得到在昨收盘基础上的跌停价
     (对于复权序列K线,由于复权处理,根据前一天的收盘价计算结果可能与跌停价不符)
    比如:
     DTPrice(REF(QHJSJ,1),0.6),得到期货的跌停价(跌停比例为0.6的话)
    :param X:
    :param A:
    :return:
    """
    return REF(X, 1) * (1 - A)


def USERFUNC0():
    """
    调用DLL中的函数,DLL中内部对应定义.
    用法:
     USERFUNC0
    :return:
    """
