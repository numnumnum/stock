import math

from .MyTT import *


# AVEDEV(X,N)

def DEVSQ(X, N):
    """
    DEVSQ(X,N) 返回数据偏差平方和
    :param X: 序列X
    :param N: 周期N,整型
    :return: 序列
    """

    def mse(li):
        _mean = np.mean(li)
        total = 0
        for i in li:
            total += (i - _mean) ** 2

        return total

    return pd.Series(X).rolling(N).apply(mse).values


# FORCAST(X, N)
# SLOPE(X, N)
# STD(X, N)
def STDP(X, N):
    """
    STDP(X,N) 返回总体标准差,N支持变量
    :param X:
    :param N:
    :return:
    """

    return pd.Series(X).rolling(N).std(ddof=0).values


def STDDEV(X, N):
    """
    STDDEV(X,N) 返回标准偏差
    :param X:
    :param N:
    :return:
    """

    def std_dev(li):
        mean = np.mean(li)
        total = 0
        for i in li:
            total += (mean - i) ** 2

        return math.sqrt(total / (len(li) - 1))

    return pd.Series(X).rolling(N).std(ddof=1).values


def VAR(X, N):
    """
    VAR(X,N) 返回估算样本方差,N支持变量，除以N-1
    :param X:
    :param N:
    :return:
    """

    def var(li):
        mean = pd.Series(li).mean()
        length = len(li)
        total = 0
        for i in li:
            total += (i - mean) ** 2

        return total / (length - 1)

    return pd.Series(X).rolling(N).apply(var).values


def VARP(X, N):
    """
    VARP(X,N) 返回总体样本方差,N支持变量,除以N,注意与VAR区别
    :param X: 序列
    :param N: 周期N
    :return: 序列
    """

    def varp(li):
        mean = pd.Series(li).mean()
        length = len(li)
        total = 0
        for i in li:
            total += (i - mean) ** 2

        return total / length

    return pd.Series(X).rolling(N).apply(varp).values


def COVAR(X, Y, N) -> np.ndarray:
    """
    COVAR(X,Y,N) 返回X和Y的N周期的协方差,N支持变量
    :param X: 序列x
    :param Y: 序列y
    :param N: 周期N，整数
    :return: 序列
    """

    def cov(lx, ly):
        """
        计算协方差
        :param lx: 序列x
        :param ly: 序列y
        :return: 协方差，数值
        """
        x_mean = pd.Series(lx).mean()
        y_mean = pd.Series(ly).mean()
        total = 0

        for x, y in zip(lx, ly):
            total += (x - x_mean) * (y - y_mean)

        return total / (len(lx) - 1)

    length = len(X)
    result = [0] * length

    for i in range(0, length - N + 1):
        _x = X.iloc[i: i + N].values
        _y = Y.iloc[i: i + N].values
        result[i + N - 1] = cov(_x, _y)

    return np.Array(result)


def RELATE(X, Y, N):
    """
    RELATE(X,Y,N) 返回X和Y的N周期的相关系数,N支持变量
    :param X: 序列x
    :param Y: 序列y
    :param N: 周期n，整型
    :return: X,Y相关系数的序列
    """
    return pd.Series(X).rolling(N).corr(Y).values


def BETA(A, B, N):
    """
    β(Beta)系数
    BETA(N) 返回当前证券N周期收益与对应大盘指数收益相比的贝塔系数,N支持变量
    (对应指数的数据必须要先下载到本地)
    :param A: 证券A序列
    :param B: 大盘B序列
    :param N: 周期N，整型
    :return: Beta系数的序列
    """


def BETAEX(X, Y, N):
    """
    长度为2时
    BETAEX(X,Y,N) 返回X与Y的N周期的相关放大系数,N支持变量
    :param X:
    :param Y:
    :param N:
    :return:
    """


def IVOLAT(N, M):
    """
    IVOLAT(N,M) 返回期权波动率 N表示计算周期 M表示类型,
    0:标的证券的历史波动率 1:隐含波动率
    :param N:
    :param M:
    :return:
    """


def BLOCKSETNUM(X):
    """
    BLOCKSETNUM.横向统计
    通达信用法:
     BLOCKSETNUM(板块名称),
    返回该板块的股票个数.
    板块名称支持系统板块(上证Ａ股,深证Ａ股,沪深Ａ股,创业板,自选股,临时条件股,板块指数等),
    地区,行业,概念,风格,指数,组合和自定义板块,注意名称要准确(此函数计算有较大耗时,请慎用于画线指标),
    第一个参数也可以是HYBLOCK,某板块指数的STKNAME
    :param X: 板块的数据,需要传板块的基本数据
    :return: 常数序列
    """
    return np.full_like(X, fill_value=len(X))


def HORCALC(module_name, data_s, calculate_s, weight):
    """
    HORCALC.横向统计
    用法:
     HORCALC(板块名称,数据项,计算方式,权重),

    数据项:
     100-HIGH,101-OPEN,102-LOW,103-CLOSE,104-VOL,105-涨幅,106-成交额
    计算方式:
     0-累加,1-排名次,2-平均值
    平均值权重:
     0-总股本,1-流通股本,2-等同权重,3-流通市值,4-总市值.

    板块名称支持系统板块(上证Ａ股,深证Ａ股,沪深Ａ股,创业板,自选股,临时条件股,板块指数等),
    地区,行业,概念,风格,指数,组合和自定义板块,注意名称要准确
    (此函数计算有较大耗时,请慎用于画线指标),
    第一个参数也可以是HYBLOCK,某板块指数的STKNAME
    :param module_name:
    :param data_s:
    :param calculate_s:
    :param weight:
    :return:
    """


def INSORT(module_name, indicator_name, indicator_line, up_down, ts_code):
    """
    INSORT.横向统计
    用法:INSORT(板块名称,指标名称,指标线,升降序),
    返回该股在板块中的排序序号
    例如:
     INSORT('房地产','KDJ',3,0)表示该股的KDJ指标第三个输出即J之值在房地产板块中的排名,
    最后一个参数为0表示降序排名.

    板块名称支持系统板块(上证Ａ股,深证Ａ股,沪深Ａ股,创业板,自选股,临时条件股,板块指数等),
    地区,行业,概念,风格,指数,组合和自定义板块,注意名称要准确(此函数计算有较大耗时,请慎用于画线指标),
    第一个参数也可以是HYBLOCK,某板块指数的STKNAME
    :param ts_code: 股票代码
    :param module_name: 板块名称
    :param indicator_name: 指标名称
    :param indicator_line: 指标线
    :param up_down: 升降序
    :return:
    """
    # 1、获取板块下所有的个股及其最高价high,最低价low,开盘价open，收盘价close,股票代码,板块名称
    # 2、获取指标函数
    indicator = getattr(MyTT, indicator_name)
    # 3、获取指标函数需要的参数
    params = indicator.__code__.co_varnames
    # 4、根据步骤1的数据计算指标
    # 5、将计算好的指标按照 indicator_line 和up_down返回ts_code股票对应的值
    # cal = indicator(params)[indicator_line]


def INSUM(module_name, indicator_name, indicator_line, calculate_type):
    """
    INSUM.横向统计
    用法:INSUM(板块名称,指标名称,指标线,计算类型),
    返回板块各成分该指标相应输出按计算类型得到的计算值.计算类型:0-累加,1-平均数,2-最大值,3-最小值.
    例如:
     INSUM('房地产','KDJ',3,0)表示房地产板块中所有股票的KDJ指标第三个输出即J之值的累加值.

    板块名称支持系统板块(上证Ａ股,深证Ａ股,沪深Ａ股,创业板,自选股,临时条件股,板块指数等),
    地区,行业,概念,风格,指数,组合和自定义板块,注意名称要准确(此函数计算有较大耗时,请慎用于画线指标),
    第一个参数也可以是HYBLOCK,某板块指数的STKNAME
    :param module_name:
    :param indicator_name:
    :param indicator_line:
    :param calculate_type:
    :return:
    """
