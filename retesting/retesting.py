from collections import namedtuple

from harmonic import *


class ReTesting:
    def __init__(self, original_data=None, retesting_data=None):
        if original_data is not None:
            self.retesting_data = retesting_data
        else:
            self.retesting_data = pd.DataFrame()

        if original_data is not None:
            self.original_data = original_data
        else:
            self.original_data = pd.DataFrame()

    def read_original_data(self, file_path=None, index='date'):
        data = pd.read_csv(file_path)
        data.set_index(index, inplace=True)
        self.original_data = data

    def read_retesting_data(self, file_path=None):
        self.retesting_data = pd.read_csv(file_path)

    def buy_points(self, retesting_data=None):
        """
        d_position为买点
        :param retesting_data: 回测数据
        :return:
        """
        if retesting_data:
            self.retesting_data = retesting_data
        cols = ['type', 'd_position', 'd', 'sale1', 'sale2', 'sale3']
        data = self.retesting_data[cols]
        BuySalePoints = namedtuple('BuySalePoints', cols)
        all_points = []
        for val in data.values:
            point = BuySalePoints(*val)
            all_points.append(point)

        return all_points

    def retesting(self, points, amount=1e5):
        """
        回测方法
        :param points: 买点的列表
        :param amount:
        :return:
        """
        stock_amount = 0
        loaded_points = []
        cur_nums = 0

        total = 0.
        right = 0.
        wrong = 0.

        c = 0
        for cyc in self.original_data.index.values:
            c += 1
            cur_date = cyc
            low_price = self.original_data['low'].loc[cyc]
            high_price = self.original_data['high'].loc[cyc]
            close_price = self.original_data['close'].loc[cyc]

            if loaded_points:
                for p in loaded_points:
                    if p.sale1 >= close_price:
                        print(f'{c}'.center(100, '*'))
                        print(f'止损：{low_price * cur_nums}')
                        wrong += 1
                        amount += low_price * cur_nums
                        cur_nums = 0
                        stock_amount = 0
                        loaded_points.remove(p)
                        print(f'sale1 {cur_date}: amount: {amount}, stock_amount: {stock_amount}')
                    elif high_price >= p.sale3:
                        print(f'{c}'.center(100, '*'))
                        print(f'止盈：{low_price * cur_nums}')
                        right += 1
                        amount += p.sale3 * cur_nums
                        cur_nums = 0
                        stock_amount = 0
                        loaded_points.remove(p)
                        print(f'sale2 {cur_date}: amount: {amount}, stock_amount: {stock_amount}')

            for point in points:
                if int(cyc) == int(point.d_position):
                    if int(amount // point.d) == 0:
                        continue
                    else:
                        cur_nums += int(amount // point.d)
                    total += 1
                    stock_amount = point.d * cur_nums
                    amount -= stock_amount
                    print(f'{c}'.center(100, '*'))
                    print(f'买入 {cur_date} amount: {amount}, stock_amount: {stock_amount}')
                    points.remove(point)
                    loaded_points.append(point)

        print((f'amount: {amount}, stock_amount: {stock_amount}, \nright: {right/total},'
               f' wrong: {wrong/total}'))


if __name__ == '__main__':
    ori_data = read_data('601012.SH', '20000101')
    retesting = ReTesting(original_data=ori_data)
    # retesting.read_original_data()
    retesting.read_retesting_data('../rs.csv')
    ps = retesting.buy_points()
    retesting.retesting(ps)
