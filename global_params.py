import platform

from utils import TushareData

token2 = "818878b9d9b32445d45f25168cf4d8fc0d0374db2b70d06067e592df"
td = TushareData(token2)

# 初始化数据库连接:
# 用户名:密码@localhost:端口/数据库名
sys_platform = platform.platform().lower()

if sys_platform.startswith('windows'):
    conn = 'postgresql+psycopg2://postgres:admin@localhost:5432/adata'
    back_conn = 'postgresql+psycopg2://postgres:admin@localhost:5432/postgres'
else:
    conn = 'postgresql+psycopg2://postgres:admin@192.168.10.45:5432/adata'
    back_conn = 'postgresql+psycopg2://postgres:admin@192.168.10.45:5432/postgres'

a_trade_dates = td.trade()

if __name__ == '__main__':
    rs = td.get_index_stock_daily('601012.SH')
    print(rs)
