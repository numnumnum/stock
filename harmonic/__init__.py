import pandas as pd
import tushare as ts
from tqdm import tqdm

from .bat_models import *
from .butterfly_models import *
from .crab_models import *
from .five_zero_models import *
from .gartley_models import *
from .shark_models import *


def read_data(code, start_date='20100101'):
    """
    tushare 获取数据
    :param code: 数据代码
    :param start_date: 数据的起始日期
    :return: 数据的dataframe
    """
    token2 = "818878b9d9b32445d45f25168cf4d8fc0d0374db2b70d06067e592df"
    ts.set_token(token2)
    pro = ts.pro_api()
    data = pro.daily(ts_code=code, start_date=start_date, end_date='20280718')
    data = data.loc[data.index[::-1]]
    data.set_index('trade_date', inplace=True)
    data = data[['open', 'close', 'high', 'low']]
    return data


def slide_window(data, size=20, step=5):
    """
    在序列上滑动窗口，生成数据
    如： data = [1, 2, 3, 4, 5], size=3
    结果： [[1, 2, 3], [2, 3, 4], [3, 4, 5]]
    :param step: 取数步长
    :param data: dataframe
    :param size: 窗口大小
    :return:
    """
    ln = len(data)
    data_list = []
    for k in range(0, ln - size + 1, step):
        data_list.append(data.iloc[k: k + size])

    return data_list


def date_range_find(data, cls=None, size=10, delta_b=0.1, delta_d=0.1, step=5):
    """
    指定数据长度为10，遍历找图形
    :param step: 取数步长
    :param cls: 谐波类
    :param delta_b: b点波动比例d
    :param delta_d: d点波动比例
    :param data:  list or series
    :param size: 窗口大小
    :return: 满足图形的条件的日期
    """
    datas = slide_window(data, size=size, step=step)
    ans = []
    for d in datas:
        instance = cls(d, delta_b=delta_b, delta_d=delta_d)
        dates = instance.dates()
        if dates:
            ans.append(dates)
    return ans


def run(data, cls=None, start=10, end=40, delta_b=0.1,
        delta_d=0.1, cols=('low', 'high'), step=5):
    """
    遍历start至end的
    :param step: 取数步长
    :param cols: 取的列
    :param cls: 计算图的类
    :param delta_d: d允许波动的范围
    :param delta_b: b允许波动的范围
    :param end: 最小窗口
    :param start: 最大窗口
    :param data: 输入数据dataframe
    :return:
    """
    data = data[list[cols]]
    result = pd.DataFrame()
    for j in tqdm(range(start, end)):
        ss = date_range_find(data, cls=cls, size=j, delta_d=delta_d, delta_b=delta_b, step=step)
        if result.empty and ss:
            result = pd.DataFrame(ss)
        elif ss:
            df = pd.DataFrame(ss)
            result = pd.concat([df, result], axis=0)

    result = result.drop_duplicates(subset=['d_position', 'type'])
    return result
