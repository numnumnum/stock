import time
from abc import abstractmethod


def time2date(t):
    time_local = time.localtime(t)
    # 转换成新的时间格式(精确到秒)
    # "%Y-%m-%d %H:%M:%S"
    return time.strftime("%Y-%m-%d %H%M", time_local)


class BaseUp:
    """
    计算图的类, 看涨基类
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def __init__(self, in_data, delta_b=0.1, delta_d=0.1):
        """
        :param in_data: dataframe
        :param delta_b: b的波动幅度
        :param delta_d: d的波动幅度
        """
        self.col1, self.col2 = in_data.columns
        self.data = in_data
        self.mid = len(in_data) // 2
        self.delta_b = delta_b
        self.delta_d = delta_d

        self.x = 0
        self.x_position = 0
        self.x_type = ''

        self.a = 0
        self.a_position = 0
        self.a_type = ''

        self.b = 0
        self.b_position = 0
        self.b_type = ''

        self.c = 0
        self.c_position = 0
        self.c_type = ''

        self.d = 0
        self.d_position = -1
        self.d_type = ''

        self.left = self.data.iloc[:self.mid]
        self.right = self.data.iloc[self.mid:]

        self.xa = 0
        self.ab = 0
        self.bc = 0

    def find_x(self):
        """
        找到x的值和位置
        :return:
        """
        left_min = self.left.min()
        x1_min = left_min[self.col1]
        x2_min = left_min[self.col2]
        if x1_min < x2_min:
            self.x = x1_min
            self.x_type = self.col1
        else:
            self.x = x2_min
            self.x_type = self.col2

        x_index = self.left[self.x_type].argmin()
        self.x_position = self.left.index[x_index]

    def find_a(self):
        # x的右侧
        a_max = self.left.max()
        a1_max = a_max[self.col1]
        a2_max = a_max[self.col2]
        if a1_max > a2_max:
            self.a = a1_max
            self.a_type = self.col1
        else:
            self.a = a2_max
            self.a_type = self.col2

        a_index = self.left[self.a_type].argmax()
        self.a_position = self.left.index[a_index]
        self.xa = self.a - self.x

    def find_c(self):
        c_max = self.right.max()
        c1_max = c_max[self.col1]
        c2_max = c_max[self.col2]
        if c1_max > c2_max:
            self.c = c1_max
            self.c_type = self.col1
        else:
            self.c = c2_max
            self.c_type = self.col2

        c_index = self.right[self.c_type].argmax()
        self.c_position = self.right.index[c_index]

    def find_b(self):
        b_df = self.data[self.a_position: self.c_position]
        if b_df.empty:
            return
        b_min = b_df.min()
        b1_min = b_min[self.col1]
        b2_min = b_min[self.col2]

        if b1_min > b2_min:
            self.b = b2_min
            self.b_type = self.col2
        else:
            self.b = b1_min
            self.b_type = self.col1

        b_index = b_df[self.b_type].argmin()
        self.b_position = b_df.index[b_index]
        # 更新c点位置
        self.right = self.data.loc[self.b_position:]
        self.find_c()
        self.bc = self.c - self.b
        self.ab = self.a - self.b

    def find_d(self):
        # d是c点右侧最小值
        d_min = self.right[self.c_position:].min()
        d1_min = d_min[self.col1]
        d2_min = d_min[self.col2]

        if d1_min < d2_min:
            self.d = d1_min
            self.d_type = self.col1
        else:
            self.d = d2_min
            self.d_type = self.col2

        d_index = self.right[self.d_type].argmin()
        self.d_position = self.right.index[d_index]

    def verify_positions(self):
        if self.x_position < self.a_position < self.b_position < self.c_position < self.d_position:
            return True
        return False

    @abstractmethod
    def verify_b(self):
        pass

    @abstractmethod
    def verify_c(self):
        pass

    @abstractmethod
    def verify_d(self):
        pass

    @abstractmethod
    def target(self) -> dict:
        return {}

    def dates(self):
        """
        :return: 返回满足条件的日期
        """
        self.find_x()
        self.find_a()
        self.find_c()
        self.find_b()
        self.find_d()

        if not self.verify_positions():
            return

        if not self.verify_c():
            return

        if not self.verify_b():
            return

        if not self.verify_d():
            return

        ans = {
            'type': 'up',
            'name': self.__class__.__name__,
            'x': self.x,
            'x_type': self.x_type,
            'x_position': self.x_position,
            'a': self.a,
            'a_type': self.a_type,
            'a_position': self.a_position,
            'b': self.b,
            'b_type': self.b_type,
            'b_position': self.b_position,
            'c': self.c,
            'c_type': self.c_type,
            'c_position': self.c_position,
            'd': self.d,
            'd_type': self.d_type,
            'd_position': self.d_position,
        }

        ans.update(self.target())
        return ans


class BaseDown:
    """
    计算图的类, 看跌基类
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def __init__(self, in_data, delta_b=0.1, delta_d=0.1):
        """
        :param in_data: dataframe
        :param delta_b: b的波动幅度
        :param delta_d: d的波动幅度
        """
        self.col1, self.col2 = in_data.columns
        self.data = in_data
        self.mid = len(in_data) // 2
        self.delta_b = delta_b
        self.delta_d = delta_d

        self.x = 0
        self.x_position = 0
        self.x_type = ''

        self.a = 0
        self.a_position = 0
        self.a_type = ''

        self.b = 0
        self.b_position = 0
        self.b_type = ''

        self.c = 0
        self.c_position = 0
        self.c_type = ''

        self.d = 0
        self.d_position = -1
        self.d_type = ''

        self.left = self.data.iloc[:self.mid]
        self.right = self.data.iloc[self.mid:]

        self.xa = 0
        self.ab = 0
        self.bc = 0

    def find_x(self):
        """
        找到x的值和位置
        :return:
        """
        left_max = self.left.max()
        x1_max = left_max[self.col1]
        x2_max = left_max[self.col2]
        if x1_max > x2_max:
            self.x = x1_max
            self.x_type = self.col1
        else:
            self.x = x2_max
            self.x_type = self.col2

        x_index = self.left[self.x_type].argmax()
        self.x_position = self.left.index[x_index]

    def find_a(self):
        # x的右侧取最小值
        a_min = self.left.min()
        a1_min = a_min[self.col1]
        a2_min = a_min[self.col2]
        if a2_min > a1_min:
            self.a = a1_min
            self.a_type = self.col1
        else:
            self.a = a2_min
            self.a_type = self.col2

        a_index = self.left[self.a_type].argmin()
        self.a_position = self.left.index[a_index]
        self.xa = self.x - self.a

    def find_c(self):
        c_min = self.right.min()
        c1_min = c_min[self.col1]
        c2_min = c_min[self.col2]
        if c2_min > c1_min:
            self.c = c1_min
            self.c_type = self.col1
        else:
            self.c = c2_min
            self.c_type = self.col2

        c_index = self.right[self.c_type].argmin()
        self.c_position = self.right.index[c_index]

    def find_b(self):
        b_df = self.data[self.a_position: self.c_position]
        if b_df.empty:
            return
        b_max = b_df.max()
        b1_max = b_max[self.col1]
        b2_max = b_max[self.col2]

        if b1_max > b2_max:
            self.b = b1_max
            self.b_type = self.col1
        else:
            self.b = b2_max
            self.b_type = self.col2

        b_index = b_df[self.b_type].argmax()
        self.b_position = b_df.index[b_index]

        self.right = self.data.loc[self.b_position:]
        self.find_c()
        self.bc = self.b - self.c
        self.ab = self.b - self.a

    def find_d(self):
        # d是c点右侧最小值
        d_max = self.right[self.c_position:].max()
        d1_max = d_max[self.col1]
        d2_max = d_max[self.col2]

        if d1_max > d2_max:
            self.d = d1_max
            self.d_type = self.col1
        else:
            self.d = d2_max
            self.d_type = self.col2

        d_index = self.right[self.d_type].argmax()
        self.d_position = self.right.index[d_index]

    def verify_positions(self):
        if self.x_position < self.a_position < self.b_position < self.c_position < self.d_position:
            return True
        return False

    @abstractmethod
    def verify_b(self):
        pass

    @abstractmethod
    def verify_c(self):
        pass

    @abstractmethod
    def verify_d(self):
        pass

    @abstractmethod
    def target(self) -> dict:
        return {}

    def dates(self):
        """
        :return: 返回满足条件的日期
        """
        self.find_x()
        self.find_a()
        self.find_c()
        self.find_b()
        self.find_d()

        if not self.verify_positions():
            return

        if not self.verify_c():
            return

        if not self.verify_b():
            return

        if not self.verify_d():
            return

        ans = {
            'type': 'down',
            'name': self.__class__.__name__,
            'x': self.x,
            'x_type': self.x_type,
            'x_position': self.x_position,
            'a': self.a,
            'a_type': self.a_type,
            'a_position': self.a_position,
            'b': self.b,
            'b_type': self.b_type,
            'b_position': self.b_position,
            'c': self.c,
            'c_type': self.c_type,
            'c_position': self.c_position,
            'd': self.d,
            'd_type': self.d_type,
            'd_position': self.d_position,
        }
        ans.update(self.target())

        return ans
