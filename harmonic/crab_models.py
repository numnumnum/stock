from .base_models import BaseUp, BaseDown


class CarbUp(BaseUp):
    """
    计算图的类, 看涨螃蟹
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def verify_b(self):
        b_min = self.a - self.xa * (0.618 + self.delta_b)
        b_max = self.a - self.xa * (0.618 - self.delta_b)
        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        c_min = self.b + self.ab * 0.382
        # c_max 可以放宽到a点位置
        c_max = self.b + self.ab * 0.886

        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min1 = self.c - self.xa * (1.618 + self.delta_d)
        d_max1 = self.c - self.xa * (1.618 - self.delta_d)
        cond1 = d_max1 >= self.d >= d_min1

        d_min2 = self.c - self.bc * 3.618
        d_max2 = self.c - self.bc * 2.618
        cond2 = d_max2 >= self.d >= d_min2

        cd = self.c - self.d
        cond3 = (1.27 + self.delta_d) * self.ab >= cd >= (1.27 - self.delta_d) * self.ab
        cond4 = (1.618 + self.delta_d) * self.ab >= cd >= (1.618 - self.delta_d) * self.ab

        if cond1 and (cond2 or cond3 or cond4):
            return True
        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.a - self.x
        ad = self.a - self.d
        return dict(
            sale1=self.c - xa * 2.0,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class CarbDown(BaseDown):
    """
    看跌螃蟹
    """

    def verify_b(self):
        b_min = self.a + self.xa * (0.618 - self.delta_b)
        b_max = self.a + self.xa * (0.618 + self.delta_b)
        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        c_min = self.b - self.ab * 0.886
        # c_max 可以放宽到a点位置
        c_max = self.b - self.ab * 0.382

        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min1 = self.c + self.xa * (1.618 - self.delta_d)
        d_max1 = self.c + self.xa * (1.618 + self.delta_d)
        cond1 = d_max1 >= self.d >= d_min1

        d_min2 = self.c + self.bc * 2.618
        d_max2 = self.c + self.bc * 3.618
        cond2 = d_max2 >= self.d >= d_min2

        cd = self.c - self.d
        cond3 = (1.27 + self.delta_d) * self.ab >= cd >= (1.27 - self.delta_d) * self.ab
        cond4 = (1.618 + self.delta_d) * self.ab >= cd >= (1.618 - self.delta_d) * self.ab

        if cond1 and (cond2 or cond3 or cond4):
            return True
        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.x - self.a
        ad = self.d - self.a
        return dict(
            sale1=self.c + xa * 2.0,
            sale2=self.d - ad * 0.382,
            sale3=self.d - ad * 0.618)
