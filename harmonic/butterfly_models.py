from .base_models import BaseUp, BaseDown


class ButterflyUp(BaseUp):
    """
    计算图的类, 看涨蝴蝶
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def verify_b(self):
        b_min = self.a - self.xa * (0.786 + self.delta_b)
        b_max = self.a - self.xa * (0.786 - self.delta_b)
        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        c_min = self.b + self.ab * 0.382
        # c_max 可以放宽到a点位置
        c_max = self.a

        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min1 = self.c - self.xa * 1.414
        d_max1 = self.c - self.xa * 1.272

        d_min2 = self.c - self.bc * 2.24
        d_max2 = self.c - self.bc * 1.618

        cond1 = d_max1 >= self.d >= d_min1
        cond2 = d_max2 >= self.d >= d_min2

        if cond1 and cond2:
            return True
        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.a - self.x
        ad = self.a - self.d
        return dict(
            sale1=self.c - xa * 1.414,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class ButterflyDown(BaseDown):
    """
    看跌蝴蝶
    """

    def verify_b(self):
        b_min = self.a + self.xa * (0.786 - self.delta_b)
        b_max = self.a + self.xa * (0.786 + self.delta_b)
        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        # c_max 可以放宽到a点位置
        c_min = self.a
        c_max = self.b - self.ab * 0.382

        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min1 = self.c + self.xa * 1.272
        d_max1 = self.c + self.xa * 1.414

        d_min2 = self.c + self.bc * 1.618
        d_max2 = self.c + self.bc * 2.24

        cond1 = d_max1 >= self.d >= d_min1
        cond2 = d_max2 >= self.d >= d_min2

        if cond1 and cond2:
            return True
        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.x - self.a
        ad = self.d - self.a
        return dict(
            sale1=self.c + xa * 1.414,
            sale2=self.d - ad * 0.382,
            sale3=self.d - ad * 0.618)
