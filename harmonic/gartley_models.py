from .base_models import BaseUp, BaseDown


class GartleyUp(BaseUp):
    """
    计算图的类, 看涨加特利
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def verify_b(self):
        b_min = self.a - self.xa * (0.618 + self.delta_b)
        b_max = self.a - self.xa * (0.618 - self.delta_b)

        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.a - self.b
        c_min = self.b + self.ab * 0.382
        c_max = self.b + self.ab * 0.886
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min = self.a - self.xa * (0.786 + self.delta_d)
        d_max = self.a - self.xa * (0.786 - self.delta_d)

        cd = self.c - self.d
        con3 = (d_max >= self.d >= d_min) and (self.d >= self.c - self.bc * 1.618)
        con4 = abs((self.ab - cd) / min(cd, self.ab)) <= (1 + self.delta_d)

        if con3 and con4:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        ad = self.a - self.d
        return dict(
            sale1=self.x,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class GartleyDown(BaseDown):
    """
    看跌加特利
    """

    def verify_b(self):
        b_min = self.a + self.xa * (0.618 - self.delta_b)
        b_max = self.a + self.xa * (0.618 + self.delta_b)

        if b_max >= self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.b - self.a
        c_min = self.b - self.ab * 0.886
        c_max = self.b - self.ab * 0.382
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        d_min = self.a + self.xa * (0.786 - self.delta_d)
        d_max = self.a + self.xa * (0.786 + self.delta_d)

        cd = self.d - self.c
        con3 = (d_max >= self.d >= d_min) and (self.d <= self.c + self.bc * 1.618)
        con4 = abs((self.ab - cd) / min(cd, self.ab)) <= (1 + self.delta_d)

        if con3 and con4:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        ad = self.d - self.a
        return dict(
            sale1=self.x,
            sale2=self.d - ad * 0.382,
            sale3=self.d - ad * 0.618)

