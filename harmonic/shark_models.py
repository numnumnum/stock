from .base_models import BaseUp, BaseDown


class SharkUp(BaseUp):
    """
    计算图的类, 看涨鲨鱼
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def verify_b(self):
        b_min = self.a - self.xa * 0.618
        b_max = self.a - self.xa * 0.382

        if b_max > self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.a - self.b
        c_min = self.b + self.ab * 1.13
        c_max = self.b + self.ab * 1.618
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        xc = self.c - self.x

        d_min1 = self.c - xc * 1.13
        d_max1 = self.c - xc * 0.886

        d_min2 = self.c - self.bc * 2.224
        d_max2 = self.c - self.bc * 1.618

        cond1 = d_max1 >= self.d >= d_min1
        cond2 = d_max2 >= self.d >= d_min2
        if cond1 and cond2:
            return True
        else:
            return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        ad = self.a - self.d
        return dict(
            sale1=self.x,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class SharkDown(BaseDown):
    """
    看跌鲨鱼
    """

    def verify_b(self):
        b_min = self.a + self.xa * 0.382
        b_max = self.a + self.xa * 0.618

        if b_max > self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.b - self.a
        c_min = self.b - self.ab * 1.618
        c_max = self.b - self.ab * 1.13
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        xc = self.x - self.c

        d_min1 = self.c + xc * 0.886
        d_max1 = self.c + xc * 1.13
        cond1 = d_max1 >= self.d >= d_min1

        d_min2 = self.c + self.bc * 1.618
        d_max2 = self.c + self.bc * 2.224
        cond2 = d_max2 >= self.d >= d_min2

        if cond1 and cond2:
            return True
        else:
            return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.x - self.a
        ad = self.d - self.a
        return dict(
            sale1=self.c + xa * 1.414,
            sale2=self.d - ad * 0.382,
            sale3=self.d - ad * 0.618)
