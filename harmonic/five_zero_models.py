from .base_models import BaseUp, BaseDown


class FiveZeroUp(BaseUp):

    def verify_b(self):
        b_min = self.a - self.xa * 1.618
        b_max = self.a - self.xa * 1.13

        cond = b_max >= self.b >= b_min
        if cond:
            return True

        return False

    def verify_c(self):
        c_min = self.b + self.ab * 1.618
        c_max = self.b + self.ab * 2.24

        cond = c_max >= self.c >= c_min
        if cond:
            return True

        return False

    def verify_d(self):
        d_min = self.c - self.bc * 0.618
        d_max = self.c - self.bc * 0.5
        cond = d_max >= self.d >= d_min

        cd = self.c - self.d
        cond1 = abs(cd - self.ab) / min(cd, self.ab) == self.delta_d

        if cond and cond1:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        xa = self.a - self.x
        ad = self.a - self.d
        return dict(
            sale1=self.c - xa * 1.414,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class FiveZeroDown(BaseDown):

    def verify_b(self):
        b_min = self.a + self.xa * 1.13
        b_max = self.a + self.xa * 1.618

        cond = b_max >= self.b >= b_min
        if cond:
            return True

        return False

    def verify_c(self):
        c_min = self.b - self.ab * 2.24
        c_max = self.b - self.ab * 1.618

        cond = c_max >= self.c >= c_min
        if cond:
            return True

        return False

    def verify_d(self):
        d_min = self.c + self.bc * 0.5
        d_max = self.c + self.bc * 0.618
        cond1 = d_max >= self.d >= d_min

        cd = self.c - self.d
        cond2 = abs(cd - self.ab) / min(cd, self.ab) == self.delta_d

        if cond1 and cond2:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        cd = abs(self.d - self.c)
        return dict(
            sale1=self.x,
            sale2=self.d - cd * 0.382,
            sale3=self.d - cd * 0.618)
