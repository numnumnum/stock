from .base_models import *


class BatUp(BaseUp):
    """
    计算图的类, 看涨蝙蝠
    查找顺序： x -> a -> c -> b -> d
    https://cn.tradingview.com/chart/EURUSD/dZoFlVfN/
    """

    def verify_b(self):
        b_min = self.a - self.xa * 0.618
        b_max = self.a - self.xa * 0.382

        if b_max > self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.a - self.b
        c_min = self.b + self.ab * 0.382
        c_max = self.b + self.ab * 0.886
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        # d点位于0.886附近, d最小值为 d_min
        d_min = self.c - self.xa * (0.886 + self.delta_d)
        d_max = self.c - self.xa * (0.886 - self.delta_d)

        cd = self.c - self.d

        if (cd / self.bc) >= 1.618 and d_max >= self.d >= d_min:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        ad = self.a - self.d
        return dict(
            sale1=self.x,
            sale2=self.d + ad * 0.382,
            sale3=self.d + ad * 0.618)


class BatDown(BaseDown):
    """
    看跌蝙蝠
    """

    def verify_b(self):
        b_min = self.a + self.xa * 0.382
        b_max = self.a + self.xa * 0.618

        if b_max > self.b >= b_min:
            return True
        return False

    def verify_c(self):
        self.ab = self.b - self.a
        c_min = self.b - self.ab * 0.886
        c_max = self.b - self.ab * 0.382
        if c_max >= self.c >= c_min:
            return True
        return False

    def verify_d(self):
        # d点位于0.886附近, d最小值为 d_min
        d_min = self.c + self.xa * (0.886 - self.delta_d)
        d_max = self.c + self.xa * (0.886 + self.delta_d)

        cd = self.d - self.c

        if (cd / self.bc) >= 1.618 and d_max >= self.d >= d_min:
            return True

        return False

    def target(self):
        """
        定义卖点,sale1为止损点,sale2为止盈点1,sale3为止盈点2
        :return:
        """
        ad = self.d - self.a
        return dict(
            sale1=self.x,
            sale2=self.d - ad * 0.382,
            sale3=self.d - ad * 0.618)
