import pandas as pd
from sqlalchemy import create_engine

remote = 'postgresql+psycopg2://postgres:admin@www.xiaofa.cn:5432/adata'
local = 'postgresql+psycopg2://postgres:admin@localhost:5432/postgres'
remote_engine = create_engine(remote)
local_engine = create_engine(local)

if __name__ == '__main__':
    with open('all_stocks.txt', 'r') as f:
        lines = f.readlines()

    for line in lines:
        sql = r'select * from "' + line.strip() + '"'
        print(sql)
        # exit()
        df = pd.read_sql(sql, local_engine)
        df.to_sql(line, remote_engine, index=False, if_exists='replace')
