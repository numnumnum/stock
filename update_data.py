import tqdm

from formula import *
from global_params import td, conn


# from utils import td, conn


def get_data_from_tushare(stock_name="000813.SZ",
                          start='20000101',
                          end='20300101', ):
    df = td.get_index_stock_daily(stock_name, start=start, end=end)
    if df.empty:
        print(stock_name)
    df['macd'] = MACD(df['close'])[-1]
    df['k'] = KDJ(df['close'], df['high'], df['low'])[0]
    df['d'] = KDJ(df['close'], df['high'], df['low'])[1]
    df['j'] = KDJ(df['close'], df['high'], df['low'])[2]
    df = df.fillna(method='bfill')

    stock_name = ''.join(stock_name.split('.')[::-1]).lower()
    df.to_sql(stock_name, conn, if_exists='replace')


def update_all():
    with open('all_stocks.txt', 'r') as f:
        lines = f.readlines()

    for line in tqdm.tqdm(lines):
        try:
            get_data_from_tushare(line.strip())
        except Exception as e:
            print(f'{line.strip()}')
            print(e)


if __name__ == '__main__':
    update_all()
