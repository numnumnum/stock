CELERY_BROKER_URL = 'redis://localhost:6379/0'  # 工人信息，以及任务信息
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'  # 任务结果存储
CELERY_TASK_SERIALIZER = 'json'
CELERY_TIME_ZONE = 'Asia/Shanghai'
CELERY_ENABLE_UTC = True
