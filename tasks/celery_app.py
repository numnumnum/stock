# 任务的定时配置
from datetime import timedelta

from celery.schedules import crontab

from celery import Celery

backend = 'redis://:@127.0.0.1:6379/1'
broker = 'redis://:@127.0.0.1:6379/2'

app = Celery(main=__name__,
             broker=broker,
             backend=backend,
             include=['tasks.test1']
             )

# 时区
app.conf.timezone = 'Asia/Shanghai'
# 是否使用UTC
app.conf.enable_utc = False

app.conf.beat_schedule = {
    'add-task': {
        'task': 'tasks.test1.add',
        # 'schedule': timedelta(seconds=3),
        'schedule': crontab(hour=16, day_of_week='1,2,3,4,5'),
        # 'schedule': crontab(hour=8, day_of_week=1),  # 每周一早八点
        'args': (30, 20),
    }
}
